package gusl.elastic.query;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.client.ElasticClientImpl;
import gusl.elastic.config.ElasticConfig;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 09/03/2022
 */
@CustomLog
public class SQLTester {

    @Test
    public void sqlTest() throws GUSLErrorException {
        ElasticClientImpl client = new ElasticClientImpl();
        ElasticConfig config = new ElasticConfig();
        config.setConnectionPoints("localhost");
        config.setThreads(2);
        config.setShards(1);
        config.setReplicas(1);
        config.setUsername("elastic");
        config.setPassword("Mayhem2021!");
        client.configure(config);

        SQLResponse response = client.getSQLRequest(SQLRequest.builder().query("select * from gusl-incidents").build());

        logger.info("Response {}", response);
    }
}
