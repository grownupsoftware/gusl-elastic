package gusl.elastic;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.client.ElasticClientImpl;
import gusl.elastic.config.ElasticConfig;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 26/08/2021
 */
@CustomLog
public class ElasticTester {

    @Test
    public void elasticTest() throws GUSLErrorException {
        ElasticClientImpl client = new ElasticClientImpl();
        ElasticConfig config = new ElasticConfig();
        config.setConnectionPoints("localhost");
        config.setThreads(2);
        config.setShards(1);
        config.setReplicas(1);
        config.setUsername("elastic");
        config.setPassword("Mayhem2021!");
        client.configure(config);

        logger.info("{}", client.checkConnection());

        logger.info("{}", client.getIndices("*"));
    }

    @Test
    public void daoTest() throws GUSLErrorException {

    }
}
