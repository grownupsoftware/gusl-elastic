package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.lambda.MutableInteger;
import gusl.elastic.client.ElasticClientImpl;
import gusl.elastic.client.ElasticDelegateImpl;
import gusl.elastic.config.ElasticConfig;
import gusl.query.QueryParams;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 08/03/2022
 */
@CustomLog
public class PointInTimeTester {

    @Test
    public void pointInTimeTester() {
        ElasticClientImpl client = new ElasticClientImpl();
        ElasticConfig config = new ElasticConfig();
        config.setConnectionPoints("localhost");
        config.setThreads(2);
        config.setShards(1);
        config.setReplicas(1);
        config.setUsername("elastic");
        config.setPassword("Mayhem2021!");
        client.configure(config);

        ElasticDelegateImpl delegate = new ElasticDelegateImpl();
        delegate.setClient(client);

        QuerySearchParams params = new QuerySearchParams();
        params.setParams(QueryParams.builder().limit(10).build());

        QuerySearch<LogSourceDO> querySearch = delegate.createQuerySearch(params, LogSourceDO.class);

        PointInTimeStreamGenerator<LogSourceDO> generator = new PointInTimeStreamGenerator<>("gusl-log-source_v3", querySearch);

        MutableInteger count = new MutableInteger(0);
        try {
            generator.stream().forEach(logSourceDO -> {
                if (count.incrementAndIsDivisibleBy(1000)) {
                    logger.info("{}", count.get());
                }
            });

            logger.info("Final Count {}", count.get());
        } catch (GUSLErrorException ex) {
            logger.warn("Nope", ex);
        }
    }
}
