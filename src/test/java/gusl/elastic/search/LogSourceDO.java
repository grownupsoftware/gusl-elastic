package gusl.elastic.search;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnoreNull;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author dhudson
 * @since 07/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
@ToStringIgnoreNull
public class LogSourceDO {

    public static final int VERSION = 3;

    @ElasticField(type = ESType.KEYWORD, primaryKey = true, populate = true)
    private String id;
    @ElasticField(type = ESType.KEYWORD)
    private String sendingIp;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean internal;
    @ElasticField(type = ESType.KEYWORD)
    private String description;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean coalesceEvents;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean enabled;
    @ElasticField(type = ESType.LONG)
    private Long parsingOrder;
    @ElasticField(type = ESType.DOUBLE)
    private Double averageEps;
    @ElasticField(type = ESType.TEXT)
    private List<Long> groupIds;
    @ElasticField(type = ESType.INTEGER)
    private Integer credibility;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean storeEventPayload;
    @ElasticField(type = ESType.LONG)
    private Long targetEventCollectorId;
    @ElasticField(type = ESType.LONG)
    private Long protocolTypeId;
    @ElasticField(type = ESType.INTEGER)
    private Integer languageId;
    @ElasticField(type = ESType.DATE, dateCreated = true, kibanaTimeField = true)
    private ZonedDateTime creationDate;
    @ElasticField(type = ESType.DATE)
    private ZonedDateTime lastEventTime;
    @ElasticField(type = ESType.DATE, dateUpdated = true)
    private ZonedDateTime modifiedDate;
    @ElasticField(type = ESType.KEYWORD)
    private String logSourceExtensionId;
    @ElasticField(type = ESType.KEYWORD)
    private String name;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean autoDiscovered;
    @ElasticField(type = ESType.LONG)
    private Long typeId;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean requiresDeploy;
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean gateway;
    @ElasticField(type = ESType.KEYWORD)
    private String wincollectInternalDestinationId;
    @ElasticField(type = ESType.KEYWORD)
    private String disconnectedLogCollectorId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
