package gusl.elastic.dao;

import gusl.elastic.model.ElasticIndex;
import gusl.elastic.utils.ElasticConstants;

import static gusl.elastic.dao.FooDao.INDEX;

/**
 * @author dhudson
 * @since 21/09/2021
 */
@ElasticIndex(index = INDEX, type = Foo.class, version = 2)
public class FooDao extends AbstractDAOElasticImpl<Foo> {

    public static final String INDEX = ElasticConstants.ELASTIC_INDEX_PREFIX + "foo";

    public FooDao() {
        super(Foo.class);
    }

    @Override
    public String getIndexName() {
        return INDEX;
    }
}
