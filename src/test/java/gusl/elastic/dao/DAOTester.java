package gusl.elastic.dao;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.IdGenerator;
import gusl.elastic.application.ElasticMigrate;
import gusl.elastic.client.ElasticClientImpl;
import gusl.elastic.client.ElasticDelegateImpl;
import gusl.elastic.config.ElasticConfig;
import gusl.query.DateRangeQuery;
import gusl.query.MatchQuery;
import gusl.query.QueryParams;
import lombok.CustomLog;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 21/09/2021
 */
@CustomLog
public class DAOTester {

    private static ElasticClientImpl elasticClient;
    private static FooDao theDao;
    private static ElasticDelegateImpl elasticDelegate;

    @BeforeClass
    public static void tearUp() {
        IdGenerator.setForTesting();
        elasticClient = getElasticClient();
        elasticDelegate = new ElasticDelegateImpl();
        elasticDelegate.setClient(elasticClient);

        theDao = new FooDao();
        theDao.setDelegate(elasticDelegate);
    }

    @Test
    public void testMigrate() throws GUSLErrorException {
        ElasticMigrate migrate = new ElasticMigrate();
        migrate.buildClassBasedIndices(elasticClient, true, false);
    }

    @Test
    public void testClean() throws GUSLErrorException {
        ElasticMigrate migrate = new ElasticMigrate();
        migrate.truncateAll(elasticClient, false);
    }

    @Test
    public void insert() throws GUSLErrorException {
        Foo foo = new Foo();

        //foo.setId(UUID.randomUUID().toString());
        foo.setMessage("Insert test");

//        foo.setDateCreated(LocalDateTime.now());
//        foo.setDateUpdated(LocalDateTime.now());

        theDao.insert(foo);
    }

    @Test
    public void update() throws GUSLErrorException {
        Foo foo = new Foo();
        foo.setId("2q56vqf5fg");
        foo.setMessage("I am updated");

        theDao.update(foo);
    }

    @Test
    public void getAllTest() throws GUSLErrorException {
        logger.info("{}", theDao.getAll());
    }

    @Test
    public void delete() throws GUSLErrorException {
        Foo foo = new Foo();
        foo.setId("2q56v71su3");
        theDao.delete(foo);
        getAllTest();
    }

    @Test
    public void fetch() throws GUSLErrorException {
        Foo bar = theDao.findRecord("2q56t85u7i");
        logger.info("Bar {}", bar);
    }

    @Test
    public void getOneTest() throws GUSLErrorException {
        QueryParams params = new QueryParams();
        params.addMust(MatchQuery.builder().field("id").value("2q56t87acj").build());
        logger.info("{}", theDao.get(params));

        logger.info("{}", theDao.getOne(params));

        params = new QueryParams();
        params.addMust(MatchQuery.builder().field("id").value("bar").build());
        logger.info("{}", theDao.getOne(params));
    }

    @Test
    public void rangeQueryTest() throws GUSLErrorException {
        QueryParams params = new QueryParams();
        DateRangeQuery rangeQuery = params.addDateRangeQuery("date-created");
        rangeQuery.setLt(LocalDateTime.now());
        logger.info("{}", theDao.get(params));
    }

    private static ElasticClientImpl getElasticClient() {
        ElasticClientImpl client = new ElasticClientImpl();
        ElasticConfig config = new ElasticConfig();
        config.setConnectionPoints("localhost");
        config.setThreads(2);
        config.setShards(1);
        config.setReplicas(1);
        client.configure(config);
        return client;
    }
}
