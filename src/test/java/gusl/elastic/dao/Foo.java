package gusl.elastic.dao;

import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * @author dhudson
 * @since 21/09/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Foo {

    @ElasticField(type = ESType.KEYWORD, primaryKey = true, populate = true)
    private String id;
    @ElasticField(type = ESType.MULTI)
    private String message;
    @ElasticField(type = ESType.DATE, dateCreated = true)
    private ZonedDateTime dateCreated;
    @ElasticField(type = ESType.DATE, dateUpdated = true)
    private ZonedDateTime dateUpdated;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
