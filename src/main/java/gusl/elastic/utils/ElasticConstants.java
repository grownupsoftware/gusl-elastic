package gusl.elastic.utils;

/**
 * @author dhudson
 */
public class ElasticConstants {

    /**
     * Application indexes will start with this.
     */
    public static final String ELASTIC_INDEX_PREFIX = "gusl-";
    /**
     * And end with this
     */
    public static final String VERSION_PREFIX = "_v";
    public static final String DATE_PREFIX = "_d";
    public static final String ROTATION_DATE_FORMAT = "yyyy-MM-dd";

    public static final int ELASTIC_DEFAULT_PORT = 9200;
    public static final int KIBANA_DEFAULT_PORT = 5601;

    public static int MAX_FIELD_LENGTH = 32766;

    public static int MAX_SEARCH_RECORDS = 10000;

    public static final String ALL_WILD_CARD = "*";
    public static final String GUSL_WILD_CARD = ELASTIC_INDEX_PREFIX + "*";

    public static final String DEFAULT_TYPE = "_doc";
    public static final String TIMESTAMP_FIELD_NAME = "@timestamp";
    public static final String DATE_CHANGED_FIELD_NAME = "date-changed";
    public static final int SCROLL_BATCH_SIZE = 200;

    public static final String NULL_VALUE = "_NULL_";
}
