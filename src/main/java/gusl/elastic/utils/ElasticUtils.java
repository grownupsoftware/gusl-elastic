package gusl.elastic.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.dates.DateMathParser;
import gusl.core.dates.GUSLDateException;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.ClassUtils;
import gusl.elastic.exception.GUSLElasticException;
import gusl.elastic.model.ElasticIndex;
import lombok.CustomLog;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author dhudson
 */
@CustomLog
public class ElasticUtils {

    private static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.getDefaultObjectMapper();

    private ElasticUtils() {
    }

    public static <T> IndexRequest createIndexRequest(String index, String id, T record) throws GUSLElasticException {
        try {
            IndexRequest indexRequest = new IndexRequest(index);
            if (id != null) {
                indexRequest.id(id);
            }
            indexRequest.source(OBJECT_MAPPER.writeValueAsBytes(record), XContentType.JSON);
            return indexRequest;
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }
    }

    public static <T> IndexRequest createIndexRequest(String index, long id, T record) throws GUSLElasticException {
        return createIndexRequest(index, Long.toString(id), record);
    }

    public static SearchRequest createSearchRequest(String... indices) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(indices);
        return searchRequest;
    }

    public static SearchRequest createFilebeatSearchRequest(SearchSourceBuilder builder, String... indices) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(indices);
        searchRequest.source(builder);
        return searchRequest;
    }

    public static SearchRequest createSearchRequest(SearchSourceBuilder builder, String... indices) {
        SearchRequest searchRequest = createSearchRequest(indices);
        searchRequest.source(builder);
        return searchRequest;
    }

    public static <T> List<T> convertResponse(SearchResponse searchResponse, Class<T> type) throws GUSLElasticException {
        Aggregations aggregations = searchResponse.getAggregations();
        if (aggregations == null) {
            return convertResponseNormal(searchResponse, type);
        } else {
            return convertResponseWithAggs(aggregations, type);
        }
    }

    private static <T> List<T> convertResponseNormal(SearchResponse searchResponse, Class<T> type) throws GUSLElasticException {
        int size = (int) searchResponse.getHits().getTotalHits().value;
        List<T> result = new ArrayList<>(size);

        if (size == 0) {
            return result;
        }

        SearchHit[] searchHits = searchResponse.getHits().getHits();
        try {
            for (SearchHit hit : searchHits) {
                // do something with the SearchHit
                result.add(OBJECT_MAPPER.readValue(hit.getSourceAsString(), type));
            }
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }

        return result;
    }

    private static <T> List<T> convertResponseWithAggs(Aggregations aggregations, Class<T> type) throws GUSLElasticException {
        List<T> result = new ArrayList<>();
        for (Aggregation aggregation : aggregations.asList()) {
            try {
                for (Terms.Bucket bucket : ((Terms) aggregation).getBuckets()) {
                    String json = "{ \"" + aggregation.getName() + "\": \"" + bucket.getKeyAsString() + "\" }";
                    result.add(OBJECT_MAPPER.readValue(json, type));
                }
            } catch (IOException ex) {
                throw new GUSLElasticException(ex);
            }
        }
        return result;
    }

    public static <T> List<Pair<T, SearchHit>> convertResponseWithHits(SearchResponse searchResponse, Class<T> type) throws GUSLElasticException {
        int size = (int) searchResponse.getHits().getTotalHits().value;
        List<Pair<T, SearchHit>> result = new ArrayList<>(size);

        if (size == 0) {
            return result;
        }

        SearchHit[] searchHits = searchResponse.getHits().getHits();
        try {
            for (SearchHit hit : searchHits) {
                // do something with the SearchHit
                result.add(Pair.of(OBJECT_MAPPER.readValue(hit.getSourceAsString(), type), hit));
            }
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }

        return result;
    }

    public static <T> List<T> convertHits(SearchHit[] searchHits, Class<T> type) throws GUSLElasticException {
        List<T> result = new ArrayList<>(searchHits.length);

        if (searchHits.length == 0) {
            return result;
        }
        try {
            for (SearchHit hit : searchHits) {
                // do something with the SearchHit
                result.add(OBJECT_MAPPER.readValue(hit.getSourceAsString(), type));
            }
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }

        return result;

    }

    public static SearchRequest getPagedSearchRequest(String index, SortBuilder<?> sortBuilder, BoolQueryBuilder boolQueryBuilder,
                                                      int offset, int perPage) throws GUSLErrorException {
        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .from(offset * perPage)
                .size(perPage);

        if (sortBuilder != null) {
            searchBuilder.sort(sortBuilder);
        }

        return new SearchRequest()
                .indices(index)
                .source(searchBuilder);
    }

    public static List<Class<?>> getIndexAnnotatedClasses() {

        Set<Class<?>> result = ClassUtils.getAnnotatedClasses(ElasticIndex.class);
        List<Class<?>> sorted = new ArrayList<>(result);

        Collections.sort(sorted, Comparator.comparing(Class::getName));

        return sorted;
    }

    public static RangeQueryBuilder createRangeQueryBuilderDayTruncated(String field, Date from, Date to) {
        RangeQueryBuilder queryDateRange = QueryBuilders.rangeQuery(field);

        if (from != null) {
            try {
                queryDateRange = queryDateRange.from(DateMathParser.parse("/d", from), true);
            } catch (GUSLDateException ex) {
                logger.warn("Unable to parse date from {}", from, ex);
            }
        }

        if (to != null) {
            try {
                // Add a day, truncate it, then take off 1 Millisecond to make it the end of the day
                queryDateRange = queryDateRange.to(DateMathParser.parse("+1d/d", to), true);
            } catch (GUSLDateException ex) {
                logger.warn("Unable to parse date to {}", to, ex);
            }
        }

        return queryDateRange;
    }

    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    public static String getElasticFieldName(Field field) {
        JsonProperty property = field.getAnnotation(JsonProperty.class);
        if (property != null) {
            return property.value();
        }

        return field.getName();
    }
}
