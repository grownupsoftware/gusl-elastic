package gusl.elastic.metadata;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.security.InvalidOperationException;
import gusl.core.security.ObfuscateMethod;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.security.PasswordStorage;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.StringUtils;
import gusl.elastic.errors.ElasticErrors;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.ElasticIndex;
import gusl.query.QueryParams;
import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;

import java.lang.reflect.Field;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author dhudson
 * @since 21/09/2021
 */
@Getter
@Setter
public class DOMetadata<T> {

    private ElasticField elasticPrimaryKey;
    private Field javaPrimaryKey;
    private Field createdField;
    private Field updatedField;
    private String kibanaTimeField;
    private boolean createRequiresUpdate;
    private boolean populate;
    private final Class<?> theDOClass;
    private String index;
    final private List<FieldTuple> securityFields = new ArrayList<>(0);
    final private List<FieldTuple> defaultFields = new ArrayList<>(0);

    public DOMetadata(Class<?> doClass) {
        theDOClass = doClass;
    }

    public void create(ElasticIndex indexAnnotation) {
        Set<Field> javaFields = ClassUtils.getFieldsFor(theDOClass, true);
        for (Field field : javaFields) {
            ElasticField elasticField = field.getAnnotation(ElasticField.class);
            if (elasticField == null) {
                // Transient field, do not map to
                continue;
            }

            if (elasticField.securityMethod() != ObfuscateMethod.NONE) {
                securityFields.add(new FieldTuple(field, elasticField));
            }

            if (StringUtils.isNotBlank(elasticField.defaultValue())) {
                defaultFields.add(new FieldTuple(field, elasticField));
            }

            if (elasticField.primaryKey()) {
                javaPrimaryKey = field;
                populate = elasticField.populate();
                elasticPrimaryKey = elasticField;
            }

            if (elasticField.dateCreated()) {
                createdField = field;
            }
            if (elasticField.dateUpdated()) {
                updatedField = field;
                if (elasticField.populate()) {
                    createRequiresUpdate = true;
                }
            }

            if (elasticField.kibanaTimeField()) {
                kibanaTimeField = field.getName();
            }
        }

        index = indexAnnotation.index();
    }

    public QueryParams createQueryParams(T entity) throws GUSLErrorException {
        try {
            QueryParams params = new QueryParams();
            params.addMust(javaPrimaryKey.getName(), javaPrimaryKey.get(entity).toString());
            params.setLimit(1);
            return params;
        } catch (IllegalAccessException ex) {
            throw ElasticErrors.AUTO_FILL_ERROR.generateException(ex);
        }
    }

    public void updateInsert(T entity) throws GUSLErrorException {
        try {
            if (createdField != null) {
                // Only set if null
                if (createdField.get(entity) == null) {
                    createdField.set(entity, ZonedDateTime.now());
                }
            }
            if (createRequiresUpdate) {
                updateUpdate(entity);
                updateDefaultFields(entity);
            }
            if (populate) {
                Object value = javaPrimaryKey.get(entity);
                if (value == null) {
                    if (elasticPrimaryKey.type() == ESType.LONG) {
                        javaPrimaryKey.set(entity, IdGenerator.generateUniqueNodeId());
                    } else {
                        javaPrimaryKey.set(entity, IdGenerator.generateUniqueNodeIdAsString());
                    }
                }
            }
            updateSecurityFields(entity);
        } catch (IllegalAccessException ex) {
            throw ElasticErrors.AUTO_FILL_ERROR.generateException(ex);
        }
    }

    private void updateDefaultFields(T entity) throws GUSLErrorException {
        for (FieldTuple tuple : defaultFields) {
            try {
                if (tuple.getJavaField().get(entity) == null) {
                    tuple.getJavaField().set(entity, tuple.getElasticField().defaultValue());
                }
            } catch (IllegalAccessException ex) {
                throw ElasticErrors.DEFAULT_ERROR.generateException(ex, tuple.getJavaField().getName(),
                        tuple.getElasticField().defaultValue());
            }
        }
    }

    public void updateUpdate(T entity) throws GUSLErrorException {
        if (updatedField != null) {
            try {
                if (updatedField.get(entity) == null) {
                    updatedField.set(entity, ZonedDateTime.now());
                }
            } catch (IllegalAccessException ex) {
                throw ElasticErrors.AUTO_FILL_ERROR.generateException(ex);
            }
        }

        updateSecurityFields(entity);
    }

    private void updateSecurityFields(T entity) throws GUSLErrorException {
        try {
            for (FieldTuple tuple : securityFields) {
                Field java = tuple.getJavaField();
                String value = (String) java.get(entity);
                if (value == null) {
                    continue;
                }

                switch (tuple.getElasticField().securityMethod()) {
                    case OBFUSCATED_WRITE:
                        if (!ObfuscatedStorage.isObfuscated(value)) {
                            java.set(entity, ObfuscatedStorage.encrypt(value));
                        }
                        break;

                    case SALTED_WRITE:
                        if (!PasswordStorage.isHashed(value)) {
                            java.set(entity, PasswordStorage.createHash(value));
                        }
                        break;
                }
            }
        } catch (IllegalAccessException | InvalidOperationException ex) {
            throw ElasticErrors.SECURITY_ERROR.generateException(ex);
        }
    }

    public IndexRequest createIndexRequest(T entity, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        IndexRequest indexRequest = new IndexRequest();
        indexRequest.setRefreshPolicy(refreshPolicy);
        indexRequest.index(index);
        updateInsert(entity);
        getIdAsString(entity).ifPresent(indexRequest::id);
        return indexRequest;
    }

    public DeleteRequest createDeleteRequest(T entity, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setRefreshPolicy(refreshPolicy);
        deleteRequest.index(index);
        getIdAsString(entity).ifPresent(deleteRequest::id);
        return deleteRequest;
    }

    public UpdateRequest createUpdateRequest(T entity, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.setRefreshPolicy(refreshPolicy);
        updateRequest.index(index);
        updateUpdate(entity);
        getIdAsString(entity).ifPresent(updateRequest::id);
        return updateRequest;
    }

    public Optional<String> getIdAsString(T entity) throws GUSLErrorException {
        try {
            if (javaPrimaryKey != null) {
                return Optional.of(javaPrimaryKey.get(entity).toString());
            }
            return Optional.empty();
        } catch (IllegalAccessException ex) {
            throw ElasticErrors.AUTO_FILL_ERROR.generateException(ex);
        }
    }

    public String getKibanaTimeField() {
        return kibanaTimeField;
    }
}
