package gusl.elastic.metadata;

import gusl.elastic.model.ElasticField;
import lombok.Getter;

import java.lang.reflect.Field;

/**
 * @author dhudson
 * @since 18/05/2022
 */
@Getter
public class FieldTuple {
    private final Field javaField;
    private final ElasticField elasticField;

    FieldTuple(Field javaField, ElasticField elasticField) {
        this.javaField = javaField;
        this.elasticField = elasticField;
    }
}
