package gusl.elastic.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.elastic.client.ElasticDelegate;
import gusl.elastic.search.QuerySearch;
import gusl.elastic.search.QuerySearchParams;
import gusl.elastic.utils.ElasticUtils;

import javax.inject.Inject;

/**
 * @author dhudson
 */
public abstract class AbstractElasticIndex {

    private static final ObjectMapper OBJECT_MAPPER = ElasticUtils.getObjectMapper();

    @Inject
    private ElasticDelegate theElasticDelegate;

    public AbstractElasticIndex() {
    }

    public ObjectMapper getMapper() {
        return OBJECT_MAPPER;
    }

    public String getIndexIdFromId(Long id) {
        return Long.toString(id);
    }

    public abstract String getIndexName();

    public <T> QuerySearch<T> createQuerySearch(QuerySearchParams params, Class<T> resultClass) {
        return theElasticDelegate.createQuerySearch(params, resultClass);
    }

    ElasticDelegate getElasticDelegate() {
        return theElasticDelegate;
    }

    public void setDelegate(ElasticDelegate delegate) {
        theElasticDelegate = delegate;
    }
}
