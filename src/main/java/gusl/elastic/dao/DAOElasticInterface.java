package gusl.elastic.dao;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dao.DAOInterface;
import gusl.query.QueryParams;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author dhudson
 * @since 11/02/2022
 */
public interface DAOElasticInterface<T> extends DAOInterface<T> {

    // Point In Time is static, and preferred
    Stream<T> stream(QueryParams params) throws GUSLErrorException;

    T insertSync(T t) throws GUSLErrorException;

    List<T> insertBatchSync(List<T> list) throws GUSLErrorException;

    T updateSync(T t) throws GUSLErrorException;

    void updateBatchSync(List<T> list) throws GUSLErrorException;

    void deleteSync(T t) throws GUSLErrorException;

    void deleteBatchSync(List<T> list) throws GUSLErrorException;

}
