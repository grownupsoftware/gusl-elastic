package gusl.elastic.dao;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.Utils;
import gusl.elastic.errors.ElasticErrors;
import gusl.elastic.exception.GUSLElasticException;
import gusl.elastic.kibana.IndexPattern;
import gusl.elastic.metadata.DOMetadata;
import gusl.elastic.model.ElasticIndex;
import gusl.elastic.search.PointInTimeStreamGenerator;
import gusl.elastic.search.QuerySearch;
import gusl.elastic.search.QuerySearchParams;
import gusl.loaders.StaticLoader;
import gusl.query.QueryOrder;
import gusl.query.QueryParams;
import gusl.query.UpdateParams;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.xcontent.XContentType;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author dhudson
 * @since 11/02/2022
 */
public abstract class AbstractDAOElasticImpl<T> extends AbstractElasticIndex implements DAOElasticInterface<T> {

    private final Class<T> theDOClass;
    private final DOMetadata<T> theMetadata;

    public AbstractDAOElasticImpl(Class<T> clazz) {
        theDOClass = clazz;
        theMetadata = new DOMetadata<>(clazz);
        theMetadata.create(this.getClass().getAnnotation(ElasticIndex.class));
    }

    @Override
    public List<T> getAll() throws GUSLErrorException {
        return createQuerySearch(createQuerySearchParams(), theDOClass).search();
    }

    @Override
    public PagedDAOResponse<T> getAllPaged(QueryParams queryParams) throws GUSLErrorException {
        QuerySearch<T> querySearch = createQuerySearch(createQuerySearchParams(queryParams), theDOClass);
        return PagedDAOResponse.<T>builder().queryParams(queryParams).content(querySearch.search()).total((int) querySearch.getTotalHits()).build();
    }

    @Override
    public List<T> get(QueryParams queryParams) throws GUSLErrorException {
        List<T> list = createQuerySearch(createQuerySearchParams(queryParams), theDOClass).search();
        if (!queryParams.isFirstAndLast()) {
            return list;
        }

        if (list.size() < 2) {
            return list;
        }

        List<T> firstLast = new ArrayList<>(2);
        firstLast.add(list.get(0));
        firstLast.add(list.get(list.size() - 1));

        return firstLast;
    }


    @Override
    public Optional<T> getOne(QueryParams queryParams) throws GUSLErrorException {
        queryParams.setLimit(1);
        List<T> found = createQuerySearch(createQuerySearchParams(queryParams), theDOClass).search();
        if (found.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(found.get(0));
    }

    @Override
    public Optional<T> findRecord(T t) throws GUSLErrorException {
        return getOne(theMetadata.createQueryParams(t));
    }

    @Override
    public <T> T findRecord(String s) throws GUSLErrorException {
        Field field = theMetadata.getJavaPrimaryKey();
        QueryParams params = new QueryParams();
        params.setLimit(1);
        params.addMust(field.getName(), s);
        Optional<T> one = (Optional<T>) getOne(params);
        if (one.isPresent()) {
            return one.get();
        }

        return null;
    }

    @Override
    public <VIEW> List<VIEW> getView(Class<VIEW> aClass, QueryParams queryParams) throws GUSLErrorException {
        QuerySearchParams querySearchParams = createQuerySearchParams(queryParams);
        querySearchParams.setView(aClass);
        return createQuerySearch(querySearchParams, aClass).search();
    }

    @Override
    public T insert(T t) throws GUSLErrorException {
        return insert(t, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public T insertSync(T t) throws GUSLErrorException {
        return insert(t, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private T insert(T t, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        getElasticDelegate().index(createIndexRequest(t, refreshPolicy));
        return t;
    }

    @Override
    public List<T> insertBatch(List<T> list) throws GUSLErrorException {
        return insertBatch(list, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public List<T> insertBatchSync(List<T> list) throws GUSLErrorException {
        return insertBatch(list, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private List<T> insertBatch(List<T> list, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        if (Utils.hasElements(list)) {
            BulkRequest bulkRequest = createBulkRequest(refreshPolicy);
            for (T entity : list) {
                bulkRequest.add(createIndexRequest(entity, WriteRequest.RefreshPolicy.NONE));
            }
            BulkResponse response = getElasticDelegate().bulk(bulkRequest);
            if (response.hasFailures()) {
                throw new GUSLElasticException(response.buildFailureMessage());
            }
        }
        return list;
    }

    @Override
    public T update(T t) throws GUSLErrorException {
        return update(t, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public T updateSync(T t) throws GUSLErrorException {
        return update(t, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private T update(T t, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        getElasticDelegate().update(createUpdateRequest(t, refreshPolicy));
        return t;
    }

    @Override
    public T update(T t, UpdateParams updateParams) throws GUSLErrorException {
        throw ElasticErrors.UPDATE_PARAMS.generateException("Not Supported");
    }

    @Override
    public void updateBatch(List<T> list) throws GUSLErrorException {
        updateBatch(list, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public void updateBatchSync(List<T> list) throws GUSLErrorException {
        updateBatch(list, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private void updateBatch(List<T> list, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        if (Utils.hasElements(list)) {
            BulkRequest bulkRequest = createBulkRequest(refreshPolicy);
            for (T entity : list) {
                bulkRequest.add(theMetadata.createUpdateRequest(entity, WriteRequest.RefreshPolicy.NONE));
            }
            BulkResponse response = getElasticDelegate().bulk(bulkRequest);
            if (response.hasFailures()) {
                throw new GUSLElasticException(response.buildFailureMessage());
            }
        }
    }

    @Override
    public void delete(T t) throws GUSLErrorException {
        delete(t, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public void deleteSync(T t) throws GUSLErrorException {
        delete(t, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private void delete(T t, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        getElasticDelegate().deleteDocument(theMetadata.createDeleteRequest(t, refreshPolicy));
    }

    @Override
    public void deleteBatch(List<T> list) throws GUSLErrorException {
        deleteBatch(list, WriteRequest.RefreshPolicy.NONE);
    }

    @Override
    public void deleteBatchSync(List<T> list) throws GUSLErrorException {
        deleteBatch(list, WriteRequest.RefreshPolicy.WAIT_UNTIL);
    }

    private void deleteBatch(List<T> list, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        if (Utils.hasElements(list)) {
            BulkRequest bulkRequest = createBulkRequest(refreshPolicy);
            for (T entity : list) {
                bulkRequest.add(theMetadata.createDeleteRequest(entity, WriteRequest.RefreshPolicy.NONE));
            }
            BulkResponse response = getElasticDelegate().bulk(bulkRequest);
            if (response.hasFailures()) {
                throw new GUSLElasticException(response.buildFailureMessage());
            }
        }
    }

    @Override
    public int deleteBatch(QueryParams queryParams) throws GUSLErrorException {
        BulkByScrollResponse bulkByScrollResponse = getElasticDelegate().deleteDocumentsByQuery(createQuerySearchParams(queryParams));
        return (int) bulkByScrollResponse.getDeleted();
    }


    @Override
    public Stream<T> stream(QueryParams params) throws GUSLErrorException {
        if (!Utils.hasElements(params.getOrderBys())) {
            // Need to add a sort so that searchAfter works
            params.addOrder(theMetadata.getJavaPrimaryKey().getName(), QueryOrder.ASC);
        }
//        if (params.getLimit() < 0) {
//            params.setLimit(QuerySearch.DEFAULT_BATCH_SIZE);
//        }

        // Batch needs to be prime number
        params.setLimit(1009);
        QuerySearch<T> querySearch = getElasticDelegate().createQuerySearch(new QuerySearchParams(params), theDOClass);
        PointInTimeStreamGenerator<T> generator = new PointInTimeStreamGenerator<>(getIndexName(), querySearch);
        return generator.stream();
    }


    @Override
    public int loadResource(String resource) throws GUSLErrorException {
        List<T> staticData = StaticLoader.loadResource(resource, theDOClass);
        insertBatch(staticData);
        return staticData.size();
    }

    @Override
    public int loadFile(File file) throws GUSLErrorException {
        List<T> staticData = StaticLoader.loadFile(file, theDOClass);
        insertBatch(staticData);
        return staticData.size();
    }

    public IndexPattern getIndexPattern() {
        return IndexPattern.of(getIndexName() + "*", theMetadata.getKibanaTimeField());
    }

    protected IndexRequest createIndexRequest(T entity, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        IndexRequest indexRequest = theMetadata.createIndexRequest(entity, refreshPolicy);
        indexRequest.setRefreshPolicy(refreshPolicy);

        try {
            indexRequest.source(getMapper().writeValueAsBytes(entity), XContentType.JSON);
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }

        return indexRequest;
    }

    private UpdateRequest createUpdateRequest(T entity, WriteRequest.RefreshPolicy refreshPolicy) throws GUSLErrorException {
        UpdateRequest updateRequest = theMetadata.createUpdateRequest(entity,refreshPolicy);
        updateRequest.setRefreshPolicy(refreshPolicy);
        try {
            updateRequest.doc(getMapper().writeValueAsBytes(entity), XContentType.JSON);
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }
        return updateRequest;
    }

    private BulkRequest createBulkRequest(WriteRequest.RefreshPolicy refreshPolicy) {
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.setRefreshPolicy(refreshPolicy);
        return bulkRequest;
    }

    private QuerySearchParams createQuerySearchParams() {
        return new QuerySearchParams().index(getIndexName());
    }

    private QuerySearchParams createQuerySearchParams(QueryParams queryParams) {
        return createQuerySearchParams().index(getIndexName()).setParams(queryParams);
    }

    public <T> QuerySearch<T> getQuerySearch(QueryParams queryParams) {
        return (QuerySearch<T>) createQuerySearch(createQuerySearchParams(queryParams), theDOClass);
    }

    public long count(QueryParams queryParams) throws GUSLErrorException {
        queryParams.setLimit(0);
        QuerySearch<T> querySearch = getQuerySearch(queryParams);
        querySearch.search();
        return querySearch.getTotalHits();
    }
}
