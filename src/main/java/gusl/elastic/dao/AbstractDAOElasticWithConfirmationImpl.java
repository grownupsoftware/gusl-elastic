package gusl.elastic.dao;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.model.WithConfirmation;

public abstract class AbstractDAOElasticWithConfirmationImpl<T extends WithConfirmation> extends AbstractDAOElasticImpl<T> implements DAOElasticInterface<T> {

    public AbstractDAOElasticWithConfirmationImpl(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T insert(T entity) throws GUSLErrorException {
        return insertSync(entity);
    }

    public T update(T entity) throws GUSLErrorException {
        return updateSync(entity);
    }
}
