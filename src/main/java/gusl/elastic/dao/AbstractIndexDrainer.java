package gusl.elastic.dao;

import com.codahale.metrics.Gauge;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.metrics.MetricsFactory;
import gusl.elastic.client.ElasticDrainer;
import gusl.elastic.exception.GUSLElasticException;
import lombok.CustomLog;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.xcontent.XContentType;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;


/**
 * Batch up Index requests and Update Requests.
 * <p>
 * Make sure that Index requests are drained before Update requests.
 * </p>
 *
 * @author dhudson
 */
@CustomLog
public abstract class AbstractIndexDrainer extends AbstractElasticIndex implements ElasticDrainer {

    private final ConcurrentLinkedQueue<IndexRequest> theIndexQueue;

    private static final int HIGH_WATER_MARK = 200;

    private static final int BATCH_SIZE = 200;

    public AbstractIndexDrainer() {
        theIndexQueue = new ConcurrentLinkedQueue<>();

        MetricsFactory.getRegistry().register(getIndexName() + ".index.queue", new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return theIndexQueue.size();
            }
        });
    }

    public void addIndexRequest(IndexRequest indexRequest) {
        theIndexQueue.add(indexRequest);
        int size = theIndexQueue.size();

        if (size % 100 == 0) {
            if (size == 1000) {
                logger.warn("Have reached high water mark of {} with a size of {} : {}, force draining", HIGH_WATER_MARK, size, indexRequest.index());
                // Force drain
                bulkDrain();
            } else {
                if (size >= HIGH_WATER_MARK) {
                    // Let get ready to grumble..................
                    logger.warn("Have reached high water mark of {} with a size of {} : {}", HIGH_WATER_MARK, size, indexRequest.index());
                }
            }
        }
    }

    @Override
    public synchronized int bulkDrain() {
        if (theIndexQueue.isEmpty()) {
            return 0;
        }

        try {
            BulkRequest bulkRequest = new BulkRequest();
            int batchCount = 0;

            Iterator<IndexRequest> it = theIndexQueue.iterator();

            while (it.hasNext()) {
                bulkRequest.add(it.next());
                it.remove();
                batchCount++;
                if (batchCount % BATCH_SIZE == 0) {
                    getElasticDelegate().bulk(bulkRequest);
                    bulkRequest = new BulkRequest();
                }
            }

            if (batchCount % BATCH_SIZE != 0) {
                // Don't do anything with an empty batch
                sendBulkRequest(bulkRequest);
            }

            logger.debug("Processed {} for {}", batchCount, getIndexName());

            return batchCount;
        } catch (GUSLErrorException ex) {
            logger.warn("Unexpected error trying to index {}", getIndexName(), ex);
        }

        return 0;
    }

    private void sendBulkRequest(BulkRequest bulkRequest) throws GUSLErrorException {
        BulkResponse response = getElasticDelegate().bulk(bulkRequest);
        if (response.hasFailures()) {
            logger.warn("Failed to index {} for reason {}", getIndexName(), response.buildFailureMessage());
        }
    }

//    public void scheduleBulkDrain(int time, TimeUnit units, ElasticDrainer drainer) {
//        getElasticClient().scheduleBulkDrain(time, units, drainer);
//    }

//    protected <T, ESO> void addRecords(String indexName, List<T> records, Class<ESO> esoClass) throws GUSLElasticException {
//        for()
//        safeStream(records).forEach(rethrowConsumer(record -> {
//            addIndexRequest(createIndexRequest(indexName, convert(record, esoClass)));
//        }));
//    }
//
//    protected <T> void addRecords(String indexName, List<T> records, Function<T, String> elasticIdExtractor) throws GUSLElasticException {
//        safeStream(records).forEach(rethrowConsumer(record -> {
//            addIndexRequest(createIndexRequest(indexName, record, elasticIdExtractor.apply(record)));
//        }));
//    }
//
//    protected <T> void addRecords(String indexName, List<T> records) throws GUSLElasticException {
//        safeStream(records).forEach(rethrowConsumer(record -> {
//            addIndexRequest(createIndexRequest(indexName, record));
//        }));
//    }
//
//    public <T, ESO extends GeoLocationESO> void addRecordsWithLocation(String indexName, List<T> records, Class<ESO> esoClass,
//                                                                       DeviceGeoLocationDataDO deviceLocationData, IpGeoLocationDataDO ipLocationData) throws GUSLErrorException {
//        for (T record : records) {
//            ESO esoRecord = convertWithLocation(record, deviceLocationData, ipLocationData, esoClass);
//            addIndexRequest(createIndexRequest(indexName, esoRecord));
//        }
//    }

    protected <T> void addRecord(String indexName, T record) throws GUSLElasticException {
        addIndexRequest(createIndexRequest(indexName, record));
    }

    public <T> IndexRequest createIndexRequest(String indexName, T record) throws GUSLElasticException {
        try {
            IndexRequest indexRequest = new IndexRequest(indexName);
            indexRequest.source(getMapper().writeValueAsBytes(record), XContentType.JSON);
            return indexRequest;
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }
    }

    public <T> void addRecord(String indexName, T record, long id) throws GUSLElasticException {
        addRecord(indexName, record, Long.toString(id));
    }

    public <T> void addRecord(String indexName, T record, String id) throws GUSLElasticException {
        addIndexRequest(createIndexRequest(indexName, record, id));
    }

    public <T> IndexRequest createIndexRequest(String indexName, T record, Long id) throws GUSLElasticException {
        return createIndexRequest(indexName, record, Long.toString(id));
    }

    public <T> IndexRequest createIndexRequest(String indexName, T record, String id) throws GUSLElasticException {
        try {
            IndexRequest indexRequest = new IndexRequest(indexName);
            indexRequest.id(id);
            indexRequest.source(getMapper().writeValueAsBytes(record), XContentType.JSON);
            return indexRequest;
        } catch (IOException ex) {
            throw new GUSLElasticException(ex);
        }
    }

    // Make sure we don't drain and do this at the same time
    public synchronized void indexNow(IndexRequest request) throws GUSLErrorException {
        getElasticDelegate().index(request);
    }

    public synchronized void bulkNow(BulkRequest bulkRequest) throws GUSLErrorException {
        BulkResponse response = getElasticDelegate().bulk(bulkRequest);
        if (response.hasFailures()) {
            logger.warn("Failed to index {} for reason {}", getIndexName(), response.buildFailureMessage());
        }
    }

//    protected <T, ESO extends GeoLocationESO> ESO convertWithLocation(T record, DeviceGeoLocationDataDO deviceLocationData, IpGeoLocationDataDO ipLocationData, Class<ESO> esoClass) {
//        ESO esoRecord = convert(record, esoClass);
//        addLocationData(esoRecord, ipLocationData, deviceLocationData);
//        return esoRecord;
//    }
//
//    protected void addLocationData(GeoLocationESO geoLocationESO, IpGeoLocationDataDO ipLocationData, DeviceGeoLocationDataDO deviceLocationData) {
//        if (deviceLocationData != null && deviceLocationData.isPresent()) {
//            geoLocationESO.setLocation(new GeoPointDO(deviceLocationData.getLatitude(), deviceLocationData.getLongitude()));
//        } else {
//            try {
//                geoLocationESO.setLocation(new GeoPointDO(Double.valueOf(ipLocationData.getLatitude()), Double.valueOf(ipLocationData.getLongitude())));
//            } catch (NumberFormatException | NullPointerException ignore) {
//                //geoLocationESO.setLocation(new GeoPointDO(DEFAULT_LAT, DEFAULT_LON));
//            }
//        }
//        if (ipLocationData != null && ipLocationData.getIpAddress() != null) {
//            geoLocationESO.setClientIp(ipLocationData.getIpAddressAsString());
//        }
//
//        //geoLocationESO.setServerIp(hostIp);
//    }
//
//    protected <T> Page<T> getPageableRecords(String index, ElasticPagedRequest pageableRequest, Class<T> responseClass) throws GUSLErrorException {
//        return getPageableRecords(index, pageableRequest, responseClass, null, null, null);
//    }


//    protected <T> Page<T> getPageableRecords(
//            String index,
//            ElasticPagedRequest pageableRequest,
//            Class<T> responseClass,
//            List<QueryBuilder> filters,
//            List<QueryBuilder> musts,
//            List<QueryBuilder> shoulds
//    ) throws GUSLErrorException {
//        try {
//            BoolQueryBuilder builder = QueryBuilders.boolQuery();
//            Date from = null;
//            Date to = null;
//            if (pageableRequest.getFromDate() != null) {
//                from = pageableRequest.getFromDate().getDate();
//            }
//            if (pageableRequest.getToDate() != null) {
//                to = pageableRequest.getToDate().getDate();
//            }
//
//            builder.filter(ElasticUtils.createRangeQueryBuilderDayTruncated(from, to));
//
//            safeStream(filters).forEach(filter -> {
//                builder.filter(filter);
//            });
//
//            safeStream(musts).forEach(must -> {
//                builder.must(must);
//            });
//
//            safeStream(shoulds).forEach(should -> {
//                builder.should(should);
//            });
//
//            safeStream(pageableRequest.getKeyValuePairs().entrySet()).forEach(entry -> {
//                builder.must(QueryBuilders.matchQuery(entry.getKey(), entry.getValue()));
//            });
//
//            FieldSortBuilder order = null;
//            if (pageableRequest.getSortColumn() != null) {
//                order = SortBuilders.fieldSort(pageableRequest.getSortColumn()).order(pageableRequest.getOrder());
//            }
//
//            SearchRequest pagedSearchRequest = ElasticUtils.getPagedSearchRequest(
//                    index,
//                    order,
//                    builder,
//                    pageableRequest.getOffset(),
//                    pageableRequest.getPerPage()
//            );
//
//            SearchResponse searchResponse = getElasticClient().search(pagedSearchRequest);
//
//            List<T> result = ElasticUtils.convertResponse(searchResponse, responseClass);
//            // Can't see why result.size() would be any different to searchResponse.getHits
//            SearchHits hits = searchResponse.getHits();
//
//            logger.debug("Index [{}]  found [{}] of [{}] for request: {} ", index, result.size(), hits.getTotalHits(), pageableRequest);
//
//            return new ElasticPageImpl<>(result, pageableRequest.getOffset(), pageableRequest.getPerPage(), (int) hits.getTotalHits().value, (pageableRequest.getOffset() + result.size() >= (int) hits.getTotalHits().value));
//
//        } catch (Throwable t) {
//            logger.error("error", t);
//            throw new GUSLErrorException(SystemErrors.IO_EXCEPTION.getError(t.getLocalizedMessage()));
//        }
//    }


}
