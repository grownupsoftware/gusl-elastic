package gusl.elastic.kibana;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 06/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Attributes {

    private String title;
    private String timeFieldName;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
