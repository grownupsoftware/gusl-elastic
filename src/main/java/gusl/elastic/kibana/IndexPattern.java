package gusl.elastic.kibana;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 06/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class IndexPattern {

    private Attributes attributes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public static IndexPattern of(String pattern, String field) {
        IndexPattern indexPattern = new IndexPattern();
        Attributes attributes = new Attributes();
        attributes.setTitle(pattern);
        attributes.setTimeFieldName(field);
        indexPattern.setAttributes(attributes);
        return indexPattern;
    }
}
