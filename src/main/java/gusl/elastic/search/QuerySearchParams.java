package gusl.elastic.search;

import gusl.core.utils.ClassUtils;
import gusl.core.utils.Utils;
import gusl.elastic.utils.ElasticConstants;
import gusl.elastic.utils.ElasticUtils;
import gusl.query.*;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static gusl.core.utils.Utils.safeCollection;
import static java.util.stream.Collectors.toCollection;

/**
 * @author dhudson
 * @since 22/09/2021
 */
public class QuerySearchParams {
    private static final String ID_FIELD_NAME = "_id";

    private final SearchSourceBuilder searchBuilder;
    private final List<String> index = new LinkedList<>();
    private QueryParams queryParams;

    public QuerySearchParams() {
        searchBuilder = new SearchSourceBuilder();
        searchBuilder.from(0);
        searchBuilder.size(ElasticConstants.MAX_SEARCH_RECORDS);
    }

    public QuerySearchParams setView(Class<?> view) {
        // If it's a view, then limit the fields returned
        List<Field> fields = ClassUtils.getFieldsFor(view, false).stream().collect(toCollection(ArrayList::new));
        String[] includes = new String[fields.size()];
        for (int i = 0; i < includes.length; i++) {
            includes[i] = ElasticUtils.getElasticFieldName(fields.get(i));
        }
        searchBuilder.fetchSource(includes, null);
        return this;
    }

    public QuerySearchParams(QueryParams params) {
        this();
        queryParams = params;
    }

    public QuerySearchParams setParams(QueryParams params) {
        queryParams = params;
        return this;
    }

    public QuerySearchParams aggregation(AggregationBuilder aggregation) {
        searchBuilder.aggregation(aggregation);
        searchBuilder.size(0);
        return this;
    }

    public QueryParams createQueryParams() {
        queryParams = new QueryParams();
        return queryParams;
    }

    public int limit() {
        return searchBuilder.size();
    }

    public QuerySearchParams index(String index) {
        this.index.add(index);
        return this;
    }

    public SearchRequest getSearchRequest() {
        searchBuilder.query(generateQueryBuilder());
        return ElasticUtils.createSearchRequest(searchBuilder, getIndices());
    }

    public String getIndex() {
        return index.get(0);
    }

    public String[] getIndices() {
        return index.toArray(new String[index.size()]);
    }

    public void clearIndex() {
        index.clear();
        getSearchRequest();
    }

    public QuerySearchParams addUniqueSort() {
        searchBuilder.sort(ID_FIELD_NAME, SortOrder.ASC);
        return this;
    }

    public QueryBuilder generateQueryBuilder() {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

        if (queryParams == null) {
            return queryBuilder;
        }

        for (MatchQuery query : safeCollection(queryParams.getMusts())) {
            queryBuilder.must(getQueryBuilderFor(query));
        }

        for (MatchQuery query : safeCollection(queryParams.getMustNots())) {
            queryBuilder.mustNot(getQueryBuilderFor(query));
        }

        for (MatchQuery query : safeCollection(queryParams.getShould())) {
            queryBuilder.should(getQueryBuilderFor(query));
        }

        for (InQuery inQuery : safeCollection(queryParams.getIns())) {
            queryBuilder.must(QueryBuilders.termsQuery(inQuery.getField(), inQuery.getValues()));
        }

        for (RangeQuery rangeQuery : safeCollection(queryParams.getRangeQueries())) {
            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(rangeQuery.getField());
            if (rangeQuery.isBetween()) {
                rangeQueryBuilder.from(rangeQuery.getFrom()).to(rangeQuery.getTo());
            } else if (rangeQuery.getLt() != null) {
                rangeQueryBuilder.lte(rangeQuery.getLt());
            } else if (rangeQuery.getGt() != null) {
                rangeQueryBuilder.gte(rangeQuery.getGt());
            }
            queryBuilder.filter(rangeQueryBuilder);
        }

        for (OrderBy order : safeCollection(queryParams.getOrderBys())) {
            if (order.getOrder() == QueryOrder.ASC) {
                searchBuilder.sort(order.getField(), SortOrder.ASC);
            } else {
                searchBuilder.sort(order.getField(), SortOrder.DESC);
            }
        }

        if (Utils.hasElements(queryParams.getDistinct())) {
            searchBuilder.size(0);
            for (String distinct : queryParams.getDistinct()) {
                searchBuilder.aggregation(AggregationBuilders.terms(distinct).field(distinct));
            }
        } else {
            if (queryParams.getLimit() != -1) {
                searchBuilder.size(queryParams.getLimit());
            }

            searchBuilder.from(queryParams.getSkip());
        }

        return queryBuilder;
    }

    private QueryBuilder getQueryBuilderFor(MatchQuery query) {
        if (query.getValue().toString().contains("*")) {
            return QueryBuilders.wildcardQuery(query.getField(), query.getValue().toString());
        }

        return QueryBuilders.termQuery(query.getField(), query.getValue());
    }

    SearchSourceBuilder getSearchBuilder() {
        return searchBuilder;
    }
}
