package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.client.ElasticClient;
import gusl.elastic.utils.ElasticUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregations;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author dhudson
 * @since 22/09/2021
 */
public class QuerySearch<T> {

    public static final int DEFAULT_BATCH_SIZE = 400;
    private int batchSize;
    private final Class<T> theResultClass;
    private SearchResponse theSearchResponse;
    private QuerySearchParams theParams;
    private ElasticClient theElasticClient;

    public QuerySearch(Class<T> resultClass) {
        theResultClass = resultClass;
    }

    public QuerySearch(QuerySearchParams params, Class<T> resultClass) {
        this(resultClass);
        batchSize = DEFAULT_BATCH_SIZE;
        theParams = params;
    }

    public QuerySearch<T> setElasticClient(ElasticClient client) {
        theElasticClient = client;
        return this;
    }

    ElasticClient getElasticClient() {
        return theElasticClient;
    }

    public Class<T> getResultClass() {
        return theResultClass;
    }

    public QuerySearch<T> batchSize(int size) {
        batchSize = size;
        return this;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public Stream<T> searchAsStream() throws GUSLErrorException {
        return search().stream();
    }

    public List<T> search() throws GUSLErrorException {
        internalSearch();
        return ElasticUtils.convertResponse(theSearchResponse, theResultClass);
    }

    public Aggregations aggregate() throws GUSLErrorException {
        internalSearch();
        return theSearchResponse.getAggregations();
    }

    private void internalSearch() throws GUSLErrorException {
        theSearchResponse = theElasticClient.search(theParams.getSearchRequest());
    }

    public long getTotalHits() {
        if (theSearchResponse != null) {
            return theSearchResponse.getHits().getTotalHits().value;
        }
        return 0;
    }

    public SearchResponse getSearchResponse() {
        return theSearchResponse;
    }

    QuerySearchParams getParams() {
        return theParams;
    }
}
