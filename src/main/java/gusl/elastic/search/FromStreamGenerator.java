package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.elasticsearch.action.search.SearchResponse;

import java.util.stream.Stream;

import static gusl.elastic.utils.ElasticConstants.MAX_SEARCH_RECORDS;

/**
 * @param <T>
 * @author dhudson
 */
@CustomLog
public class FromStreamGenerator<T> extends AbstractStreamGenerator<T> {

    public FromStreamGenerator(QuerySearch<T> querySearch) {
        super(querySearch);
    }

    @Override
    public T next() {
        try {
            if (index >= theBatchSize) {
                // Need to get the next batch
                SearchResponse response = theClient.search(theSearchRequest.source(theSourceBuilder.from((int) total)));
                searchHits = response.getHits().getHits();
                index = 0;
            }

            T t = get(searchHits[index]);

            // Increment totals
            index++;
            total++;

            return t;
        } catch (Exception ex) {
            logger.warn("Error getting next", ex);
            return null;
        }
    }

    public Stream<T> stream() throws GUSLErrorException {
        SearchResponse response = theClient.search(theSearchRequest);

        searchTotal = response.getHits().getTotalHits().value;

        if (searchTotal == 0) {
            return Stream.empty();
        }

        if (searchTotal > MAX_SEARCH_RECORDS) {
            String index;
            if (getSearchRequest().indices().length > 0) {
                index = getSearchRequest().indices()[0];
            } else {
                index = "unknown";
            }

            logger.warn("Index {}, Too many search hits {} for this streamer, try another one, limiting result to {}",
                    index, searchTotal, MAX_SEARCH_RECORDS);
            searchTotal = MAX_SEARCH_RECORDS;
        }

        total = 0;
        index = 0;

        searchHits = response.getHits().getHits();

        return generateStream();
    }

}
