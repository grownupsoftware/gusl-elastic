package gusl.elastic.search;

import gusl.elastic.client.ElasticClient;
import gusl.elastic.utils.ElasticUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author dhudson on 23/10/2019
 */
public abstract class AbstractStreamGenerator<T> implements StreamGenerator<T>, Iterator<T> {

    final Class<T> theResultClass;
    final ElasticClient theClient;
    final SearchRequest theSearchRequest;
    final int theBatchSize;
    final int theLimit;
    SearchSourceBuilder theSourceBuilder;
    final QuerySearch<T> theQuerySearch;

    // How many through the batch
    int index = 0;
    // acc total
    long total = 0;
    // search total;
    long searchTotal = 0;

    SearchHit[] searchHits;

    public AbstractStreamGenerator(QuerySearch<T> querySearch) {
        theQuerySearch = querySearch;
        theClient = querySearch.getElasticClient();
        theResultClass = querySearch.getResultClass();
        theSearchRequest = querySearch.getParams().getSearchRequest();
        theSourceBuilder = querySearch.getParams().getSearchBuilder();
        theBatchSize = querySearch.getBatchSize();
        theLimit = querySearch.getParams().limit();
    }

    void checkSortQuery() {
        List<SortBuilder<?>> sorts = theSourceBuilder.sorts();
        // There has to be at least one sort
        if (sorts == null || sorts.isEmpty()) {
            theQuerySearch.getParams().addUniqueSort();
            theSourceBuilder = theQuerySearch.getParams().getSearchBuilder();
        }
    }

    public Class<T> getResultClass() {
        return theResultClass;
    }

    public ElasticClient getClient() {
        return theClient;
    }

    public SearchRequest getSearchRequest() {
        return theSearchRequest;
    }

    @Override
    public boolean hasNext() {
        if (theLimit > 0 && total >= theLimit) {
            return false;
        }

        return (searchTotal > total);
    }

    T get(SearchHit hit) throws IOException {
        return ElasticUtils.getObjectMapper().readValue(hit.getSourceAsString(), theResultClass);
    }

    Stream<T> generateStream() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                this, Spliterator.ORDERED | Spliterator.NONNULL), false);
    }
}
