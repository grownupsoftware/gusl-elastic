package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;

import java.util.stream.Stream;

/**
 * @author dhudson on 23/10/2019
 */
public interface StreamGenerator<T> {

    Stream<T> stream() throws GUSLErrorException;

}
