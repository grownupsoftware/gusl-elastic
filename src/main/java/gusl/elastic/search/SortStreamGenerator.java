package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;

import java.util.stream.Stream;

/**
 * To be used when the data set is more than likely to be greater than 10K.
 * <p>
 * I think that this issues the search request per batch, and send skips forward.
 * So it can't be used for searches that are going to change over time.
 * So you can't use "now" or "matchAll" in the query as the index may be added to during that time.
 * Sorts really shouldn't be used with this generator either.
 * </p><p>
 * This means on a volatile index, the total hits can change.
 * </p>
 *
 * @author dhudson on 30/08/2019
 */
@CustomLog
public class SortStreamGenerator<T> extends AbstractStreamGenerator<T> {

    private Object[] afterValues;

    public SortStreamGenerator(QuerySearch<T> querySearch) {
        super(querySearch);
    }

    @Override
    public T next() {
        try {
            if (index >= theBatchSize) {
                // Need to get the next batch
                setAfterValues(theClient.search(theSearchRequest.source(theSourceBuilder.searchAfter(afterValues))));
            }

            T t = get(searchHits[index]);

            // Increment totals
            index++;
            total++;

            return t;
        } catch (Throwable ex) {
            logger.warn("Error getting next Index {}, searchHits size {}, searchTotal {} total {}", index, searchHits.length, searchTotal, total, ex);
            return null;
        }
    }

    @Override
    public Stream<T> stream() throws GUSLErrorException {
        checkSortQuery();
        SearchResponse response = theClient.search(theSearchRequest);

        if (response.getHits().getTotalHits().value == 0) {
            return Stream.empty();
        }

        setAfterValues(response);
        total = 0;

        return generateStream();
    }

    private void setAfterValues(SearchResponse response) {
        index = 0;
        searchHits = response.getHits().getHits();

        if (searchHits.length > 1) {
            SearchHit lastHit = searchHits[searchHits.length - 1];
            afterValues = lastHit.getSortValues();
        }

        if (searchHits.length != theBatchSize) {
            // End of the run, lets adjust the total
            searchTotal = total + searchHits.length;
        } else {
            // NB: this changes over time
            searchTotal = response.getHits().getTotalHits().value;
        }
    }
}
