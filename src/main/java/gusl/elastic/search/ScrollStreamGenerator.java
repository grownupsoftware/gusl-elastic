package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.search.Scroll;

import java.util.stream.Stream;

/**
 * Use the scroll method for searching.
 * <p>
 * This does search only once, but is costly.
 * </p>
 *
 * @author dhudson on 23/10/2019
 */
@CustomLog
public class ScrollStreamGenerator<T> extends AbstractStreamGenerator<T> {

    private String theScrollId;
    private Scroll theScroll;

    public ScrollStreamGenerator(QuerySearch<T> querySearch) {
        super(querySearch);
    }

    @Override
    public boolean hasNext() {
        if (!super.hasNext()) {
            clearScroll();
            return false;
        }
        return true;
    }

    @Override
    public T next() {
        try {
            if (index >= theBatchSize) {
                // Need to get the next batch
                SearchScrollRequest next = new SearchScrollRequest(theScrollId);
                next.scroll(theScroll);

                updateValues(getClient().searchWithScroll(next));
            }

            T t = get(searchHits[index]);

            // Increment totals
            index++;
            total++;

            return t;
        } catch (Throwable ex) {
            logger.warn("Error getting next Index {}, searchHits size {}, searchTotal {} total {}", index, searchHits.length, searchTotal, total, ex);
            clearScroll();
            return null;
        }
    }

    @Override
    public Stream<T> stream() throws GUSLErrorException {
        theScroll = new Scroll(TimeValue.timeValueMinutes(10));
        theSearchRequest.scroll(theScroll);

        SearchResponse response = theClient.search(theSearchRequest);

        theScrollId = response.getScrollId();
        searchTotal = response.getHits().getTotalHits().value;

        if (searchTotal == 0) {
            clearScroll();
            return Stream.empty();
        }

        total = 0;

        updateValues(response);

        return generateStream();
    }

    private void updateValues(SearchResponse response) {
        searchHits = response.getHits().getHits();
        index = 0;
    }

    private void clearScroll() {
        try {
            getClient().clearScroll(theScrollId);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to free Scroll {}", theScrollId, ex);
        }
    }
}
