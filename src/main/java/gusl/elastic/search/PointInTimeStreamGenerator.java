package gusl.elastic.search;

import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.PointInTimeBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.util.stream.Stream;

/**
 * batch size needs to be a prime number for this to work correctly.
 *
 * @author dhudson
 * @since 08/03/2022
 */
@CustomLog
public class PointInTimeStreamGenerator<T> extends AbstractStreamGenerator<T> {

    private static final int KEEP_ALIVE = 10;

    private Object[] afterValues;
    private String pointInTimeId;
    private final String indexName;

    public PointInTimeStreamGenerator(String indexName, QuerySearch<T> querySearch) {
        super(querySearch);
        this.indexName = indexName;
    }

    @Override
    public boolean hasNext() {
        if (total < searchTotal) {
            return true;
        }
        closePointInTime();
        return false;
    }

    @Override
    public T next() {
        try {
            if (index >= theLimit) {
                // Need to get the next batch
                setAfterValues(getClient().search(theSearchRequest.source(theSourceBuilder.pointInTimeBuilder(new PointInTimeBuilder(pointInTimeId))
                        .searchAfter(afterValues))));
            }

            T t = get(searchHits[index]);

            // Increment totals
            index++;
            total++;

            return t;
        } catch (Throwable ex) {
            logger.warn("Error getting next Index {}, searchHits size {}, searchTotal {} total {}", index, searchHits.length, searchTotal, total, ex);
            closePointInTime();
            return null;
        }
    }

    @Override
    public Stream<T> stream() throws GUSLErrorException {
        //checkSortQuery();
        theSourceBuilder.sort("id", SortOrder.ASC);
        pointInTimeId = theClient.openPointInTime(indexName, KEEP_ALIVE);
        final PointInTimeBuilder builder = new PointInTimeBuilder(pointInTimeId);
        theSearchRequest.source(theSourceBuilder.pointInTimeBuilder(builder).trackTotalHits(false));

        SearchResponse response = theClient.search(theSearchRequest);

        if (response.getHits().getHits().length == 0) {
            closePointInTime();
            return Stream.empty();
        }

        total = 0;
        setAfterValues(response);
        return generateStream();
    }


    private void setAfterValues(SearchResponse response) {
        index = 0;
        searchHits = response.getHits().getHits();

        if (searchHits.length > 1) {
            SearchHit lastHit = searchHits[searchHits.length - 1];
            afterValues = lastHit.getSortValues();
            if (searchHits.length == theLimit) {
                searchTotal = total + searchHits.length + 1;
            } else {
                searchTotal = total + searchHits.length;
            }
        }
    }

    private void closePointInTime() {
        try {
            getClient().closePointInTime(pointInTimeId);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to close point in time {}", pointInTimeId, ex);
        }
    }

}
