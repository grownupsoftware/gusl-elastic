package gusl.elastic.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.config.ElasticConfig;
import gusl.elastic.exception.GUSLElasticException;
import gusl.elastic.model.ElasticAliases;
import gusl.elastic.model.Indices;
import gusl.elastic.query.SQLRequest;
import gusl.elastic.query.SQLResponse;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.jvnet.hk2.annotations.Contract;

import java.io.Closeable;
import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author dhudson
 */
@Contract
public interface ElasticClient extends Closeable {

    void configure(ElasticConfig config);

    Indices getAllIndices() throws GUSLErrorException;

    Indices getApplicationIndices() throws GUSLErrorException;

    Map<String, Set<AliasMetadata>> getAllAliases() throws GUSLErrorException;

    ElasticAliases getApplicationAliases() throws GUSLErrorException;

    boolean addAlias(String index, String alias) throws GUSLErrorException;

    boolean reindexWithSourceQuery(String sourceIndex, String destIndex, QueryBuilder sourceQuery) throws GUSLErrorException;

    boolean swapAliases(String oldIndex, String newIndex, String alias) throws GUSLErrorException;

    UpdateResponse update(UpdateRequest request) throws GUSLErrorException;

    IndexResponse index(IndexRequest request) throws GUSLErrorException;

    void indexAsync(IndexRequest request);

    SearchResponse search(SearchRequest request) throws GUSLErrorException;

    SearchResponse searchWithScroll(SearchScrollRequest request) throws GUSLErrorException;

    ClearScrollResponse clearScroll(String scrollId) throws GUSLErrorException;

    BulkResponse bulk(BulkRequest bulkRequest) throws GUSLErrorException;

    void bulkAsync(BulkRequest bulkRequest);

    void scheduleBulkDrain(int time, TimeUnit units, ElasticDrainer drainer);

    RestHighLevelClient getClient();

    GetResponse get(GetRequest request) throws GUSLErrorException;

    DeleteResponse deleteDocument(DeleteRequest request) throws GUSLErrorException;

    ForceMergeResponse forceMergeAllIndices() throws GUSLErrorException;

    ForceMergeResponse forceMergeApplicationIndices() throws GUSLErrorException;

    BulkByScrollResponse deleteDocumentsByQuery(DeleteByQueryRequest deleteRequest) throws GUSLErrorException;

    BulkByScrollResponse updateDocumentByQuery(String indexName, MatchQueryBuilder id) throws GUSLElasticException;

    boolean checkConnection();

    void waitForStartup(Duration duration) throws TimeoutException;

    String openPointInTime(String index, int minutes) throws GUSLElasticException;

    boolean closePointInTime(String pitId) throws GUSLErrorException;

    SQLResponse getSQLRequest(SQLRequest query) throws GUSLErrorException;

}
