package gusl.elastic.client;

import lombok.CustomLog;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.rest.RestStatus;

/**
 *
 * @author dhudson
 */
@CustomLog
public class LoggingActionListener implements ActionListener<IndexResponse> {

    @Override
    public void onFailure(Exception ex) {
        logger.warn(ex);
    }

    @Override
    public void onResponse(IndexResponse response) {
        if (response.status() != RestStatus.CREATED) {
            logger.warn("Possible invalid response from Elastic Search. {}:{}", response, response.status());
        }
    }

}
