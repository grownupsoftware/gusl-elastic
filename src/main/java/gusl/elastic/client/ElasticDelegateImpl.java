package gusl.elastic.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.search.QuerySearch;
import gusl.elastic.search.QuerySearchParams;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

/**
 * @author dhudson
 * @since 22/09/2021
 */
@Service
@CustomLog
@NoArgsConstructor
public class ElasticDelegateImpl implements ElasticDelegate {

    @Inject
    public ElasticClient theElasticClient;

    @Override
    public <T> QuerySearch<T> createQuerySearch(QuerySearchParams params, Class<T> resultClass) {
        QuerySearch<T> querySearch = new QuerySearch<>(params, resultClass);
        querySearch.setElasticClient(theElasticClient);
        return querySearch;
    }

    @Override
    public GetResponse get(GetRequest request) throws GUSLErrorException {
        return theElasticClient.get(request);
    }

    @Override
    public IndexResponse index(IndexRequest request) throws GUSLErrorException {
        return theElasticClient.index(request);
    }

    @Override
    public UpdateResponse update(UpdateRequest request) throws GUSLErrorException {
        return theElasticClient.update(request);
    }

    @Override
    public BulkResponse bulk(BulkRequest bulkRequest) throws GUSLErrorException {
        return theElasticClient.bulk(bulkRequest);
    }

    @Override
    public DeleteResponse deleteDocument(DeleteRequest request) throws GUSLErrorException {
        return theElasticClient.deleteDocument(request);
    }

    @Override
    public BulkByScrollResponse deleteDocumentsByQuery(QuerySearchParams queryParams) throws GUSLErrorException {
        DeleteByQueryRequest request = new DeleteByQueryRequest();
        request.indices(queryParams.getIndices());
        request.setQuery(queryParams.generateQueryBuilder());
        request.setConflicts("proceed");
        return theElasticClient.deleteDocumentsByQuery(request);
    }

    public void setClient(ElasticClient client) {
        theElasticClient = client;
    }


}
