package gusl.elastic.client;

/**
 *
 * @author dhudson
 */
public interface ElasticDrainer {

    /**
     * This gets called at the scheduled time.
     * @return how many records drained
     */
    int bulkDrain();

}
