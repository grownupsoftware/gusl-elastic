package gusl.elastic.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.search.QuerySearch;
import gusl.elastic.search.QuerySearchParams;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 22/09/2021
 */
@Contract
public interface ElasticDelegate {

    <T> QuerySearch<T> createQuerySearch(QuerySearchParams params, Class<T> resultClass);

    GetResponse get(GetRequest request) throws GUSLErrorException;

    IndexResponse index(IndexRequest request) throws GUSLErrorException;

    UpdateResponse update(UpdateRequest request) throws GUSLErrorException;

    BulkResponse bulk(BulkRequest bulkRequest) throws GUSLErrorException;

    DeleteResponse deleteDocument(DeleteRequest request) throws GUSLErrorException;

    BulkByScrollResponse deleteDocumentsByQuery(QuerySearchParams queryParams) throws GUSLErrorException;
}
