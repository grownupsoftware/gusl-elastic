package gusl.elastic.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.NamedThreadFactory;
import gusl.core.executors.ReportingThreadPoolExecutor;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import gusl.core.utils.Utils;
import gusl.elastic.config.ElasticConfig;
import gusl.elastic.exception.GUSLElasticException;
import gusl.elastic.model.ElasticAliases;
import gusl.elastic.model.Indices;
import gusl.elastic.query.QueryError;
import gusl.elastic.query.SQLRequest;
import gusl.elastic.query.SQLResponse;
import gusl.elastic.utils.ElasticConstants;
import gusl.elastic.utils.ElasticUtils;
import lombok.CustomLog;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.alias.Alias;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.xcontent.XContentType;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.ReindexRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.jvnet.hk2.annotations.Service;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * We have to wrap the high level client, as it seems to be missing all of the
 * admin features.
 * <p>
 * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/index.html
 *
 * @author dhudson
 */
@Service
@CustomLog
public class ElasticClientImpl implements ElasticClient {

    private final int NOT_FOUND_STATUS = 404;

    private RestHighLevelClient theElasticClient;
    private ElasticConfig theElasticConfig;

    private final LoggingActionListener theActionListener;
    private ScheduledExecutorService theBulkExecutor;

    public ElasticClientImpl() {
        theActionListener = new LoggingActionListener();
    }

    @Override
    public Indices getAllIndices() throws GUSLErrorException {
        return getIndices(ElasticConstants.ALL_WILD_CARD);
    }

    @Override
    public Indices getApplicationIndices() throws GUSLErrorException {
        return getIndices(ElasticConstants.GUSL_WILD_CARD);
    }

    public Indices getIndices(String wildcard) throws GUSLErrorException {
        Indices indices = new Indices();
        try {
            GetIndexRequest request = new GetIndexRequest(wildcard);
            request.includeDefaults(true).indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

            GetIndexResponse response = theElasticClient.indices().get(request, RequestOptions.DEFAULT);
            indices.populate(response);
            return indices;
        } catch (ElasticsearchStatusException ex) {
            if (ex.status().getStatus() == NOT_FOUND_STATUS) {
                indices.setIndices(Collections.emptyList());
                return indices;
            }
            logger.warn("Get indices failed", ex);
            throw new GUSLElasticException(ex);
        } catch (Throwable ex) {
            logger.warn("Get indices failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    private Map<String, Set<AliasMetadata>> getAliases(String wildcard) throws GUSLErrorException {
        try {
            GetAliasesRequest request = new GetAliasesRequest();
            request.indices(wildcard);
            request.indicesOptions(IndicesOptions.lenientExpandOpen());
            GetAliasesResponse response = theElasticClient.indices().getAlias(request, RequestOptions.DEFAULT);
            return response.getAliases();
        } catch (Throwable ex) {
            logger.warn("Get aliases failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public Map<String, Set<AliasMetadata>> getAllAliases() throws GUSLErrorException {
        return getAliases(ElasticConstants.ALL_WILD_CARD);
    }

    @Override
    public ElasticAliases getApplicationAliases() throws GUSLErrorException {
        ElasticAliases aliases = new ElasticAliases();
        aliases.populate(getAliases(ElasticConstants.GUSL_WILD_CARD));
        return aliases;
    }

    public AcknowledgedResponse deleteIndex(String name) throws GUSLElasticException {
        DeleteIndexRequest request = new DeleteIndexRequest(name);
        try {
            return theElasticClient.indices().delete(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Delete index failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public ForceMergeResponse forceMergeAllIndices() throws GUSLErrorException {
        return forceMergeIndices(ElasticConstants.ALL_WILD_CARD);
    }

    @Override
    public ForceMergeResponse forceMergeApplicationIndices() throws GUSLErrorException {
        return forceMergeIndices(ElasticConstants.GUSL_WILD_CARD);
    }

    private ForceMergeResponse forceMergeIndices(String wildcard) throws GUSLElasticException {
        ForceMergeRequest request = new ForceMergeRequest(wildcard);
        request.onlyExpungeDeletes(true);
        request.flush(true);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());

        GenericActionListener<ForceMergeResponse> listener = new GenericActionListener<>();

        try {
            theElasticClient.indices().forcemergeAsync(request, RequestOptions.DEFAULT, listener);

            listener.getCountDownLatch().await();
            if (listener.getResult()) {
                return listener.getResponse();
            }

            throw listener.getException();
        } catch (Throwable ex) {
            logger.warn("Force Merge failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public void close() {
        IOUtils.closeQuietly(theElasticClient);
        theBulkExecutor.shutdownNow();
    }

    /**
     * Create a specified index with the given schema file.
     */
    public boolean createIndex(String indexName, File schemaFile) throws GUSLErrorException {
        try {
            CreateIndexRequest request = new CreateIndexRequest(indexName);
            request.settings(createIndexSettingsBuilder());
            String schema = IOUtils.readFileAsString(schemaFile);
            request.source(schema, XContentType.JSON);
            CreateIndexResponse response = theElasticClient.indices().create(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();
        } catch (Throwable ex) {
            logger.warn("Create index failed", ex);
            throw new GUSLElasticException(ex);
        }

    }

    public boolean createIndex(String indexName, String mapping, String aliasName) throws GUSLErrorException {
        try {
            CreateIndexRequest request = new CreateIndexRequest(indexName);
            request.settings(createIndexSettingsBuilder());
            request.source(mapping, XContentType.JSON);
            if (aliasName != null) {
                request.alias(new Alias(aliasName));
            }

            CreateIndexResponse response = theElasticClient.indices().create(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();
        } catch (Throwable ex) {
            logger.warn("Create index failed mapping:[{}]", mapping, ex);
            throw new GUSLElasticException(ex);
        }

    }

    private Settings.Builder createIndexSettingsBuilder() {
        return Settings.builder().put("index.number_of_shards", theElasticConfig.getShards()).put("index.number_of_replicas", theElasticConfig.getReplicas());
    }

    @Override
    public UpdateResponse update(UpdateRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.update(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Update failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public IndexResponse index(IndexRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.index(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Index failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public void indexAsync(IndexRequest request) {
        theElasticClient.indexAsync(request, RequestOptions.DEFAULT, theActionListener);
    }

    @Override
    public void configure(ElasticConfig config) {
        String[] connections = config.getArrayOfConnectionPoints();
        logger.info("ElasticSearch - Connecting with : {}", config);

        HttpHost[] hosts = new HttpHost[connections.length];
        for (int i = 0; i < connections.length; i++) {
            hosts[i] = new HttpHost(connections[i], ElasticConstants.ELASTIC_DEFAULT_PORT, "http");
        }

        RestClientBuilder builder = RestClient.builder(hosts).setRequestConfigCallback(requestConfigBuilder -> requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(0));

        if (StringUtils.isNotBlank(config.getUsername())) {
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.getUsername(), config.getPassword()));
            builder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
        } else {
            builder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder);
        }

        builder.setCompressionEnabled(true).setStrictDeprecationMode(false);

        theElasticClient = new RestHighLevelClient(builder);

        theBulkExecutor = new ReportingThreadPoolExecutor(config.getThreads(), new NamedThreadFactory("Elastic Bulk"));

        theElasticConfig = config;
    }

    @Override
    public SearchResponse search(SearchRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.search(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Searched failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public SearchResponse searchWithScroll(SearchScrollRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.scroll(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Search with scroll failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public ClearScrollResponse clearScroll(String scrollId) throws GUSLErrorException {
        ClearScrollRequest request = new ClearScrollRequest();
        request.addScrollId(scrollId);
        try {
            return theElasticClient.clearScroll(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("clear scroll failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public BulkResponse bulk(BulkRequest bulkRequest) throws GUSLErrorException {
        try {
            return theElasticClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("bulk failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public GetResponse get(GetRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.get(request, RequestOptions.DEFAULT);
        } catch (Throwable ex) {
            logger.warn("Get failed index: {}", request.index(), ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public void bulkAsync(BulkRequest bulkRequest) {
        theElasticClient.bulkAsync(bulkRequest, RequestOptions.DEFAULT, new BulkActionListener());
    }

    public void scheduleBulkDrain(int time, TimeUnit units, ElasticDrainer drainer) {
        theBulkExecutor.scheduleWithFixedDelay(() -> {
            try {
                drainer.bulkDrain();
            } catch (Throwable t) {
                logger.warn("Exception thrown whilst trying to drain {}", drainer.getClass().getName(), t);
            }
        }, time, time, units);
    }

    @Override
    public RestHighLevelClient getClient() {
        return theElasticClient;
    }

    @Override
    public boolean reindexWithSourceQuery(String sourceIndex, String destIndex, QueryBuilder sourceQuery) throws GUSLErrorException {
        try {
            ReindexRequest request = new ReindexRequest();
            request.setDestIndex(destIndex);
            request.setSourceIndices(sourceIndex);
            request.setRefresh(true);
            if (sourceQuery != null) {
                //request.setSourceQuery(QueryBuilders.rangeQuery("@timestamp").gte("now-30d"));
                request.setSourceQuery(sourceQuery);
            }
            // Timout doesn't work, so lets do Async
            // request.setTimeout(TimeValue.timeValueHours(2));
            GenericActionListener<BulkByScrollResponse> listener = new GenericActionListener<>();

            theElasticClient.reindexAsync(request, RequestOptions.DEFAULT, listener);

            listener.getCountDownLatch().await();
            if (listener.getResult()) {
                logger.info("Reindex complete {}", listener.getResponse());
                return true;
            }

            logger.warn("Reindex failed", listener.getException());
            throw listener.getException();

        } catch (Throwable ex) {
            logger.warn("Reindex failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public boolean swapAliases(String oldIndex, String newIndex, String alias) throws GUSLErrorException {
        try {
            IndicesAliasesRequest request = new IndicesAliasesRequest();
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(IndicesAliasesRequest.AliasActions.Type.REMOVE).index(oldIndex).alias(alias));
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(IndicesAliasesRequest.AliasActions.Type.ADD).index(newIndex).alias(alias));

            AcknowledgedResponse response = theElasticClient.indices().updateAliases(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();

        } catch (Throwable ex) {
            logger.warn("Swap Aliases failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public boolean addAlias(String index, String alias) throws GUSLErrorException {
        try {
            IndicesAliasesRequest request = new IndicesAliasesRequest();
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(IndicesAliasesRequest.AliasActions.Type.ADD).index(index).alias(alias));

            AcknowledgedResponse response = theElasticClient.indices().updateAliases(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();
        } catch (Throwable ex) {
            logger.warn("Add Aliases failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    public boolean removeAlias(String index, String alias) throws GUSLErrorException {
        try {
            IndicesAliasesRequest request = new IndicesAliasesRequest();
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(IndicesAliasesRequest.AliasActions.Type.REMOVE).index(index).alias(alias));

            AcknowledgedResponse response = theElasticClient.indices().updateAliases(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();

        } catch (Throwable ex) {
            logger.warn("Remove Aliases failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public SQLResponse getSQLRequest(SQLRequest query) throws GUSLErrorException {
        try {
            long startTime = System.currentTimeMillis();
            Request request = new Request("POST", "/_sql?format=json");
            request.setJsonEntity(ElasticUtils.getObjectMapper().writeValueAsString(query));
            Response response = theElasticClient.getLowLevelClient().performRequest(request);
            SQLResponse sqlResponse = ElasticUtils.getObjectMapper().readValue(response.getEntity().getContent(), SQLResponse.class);
            sqlResponse.setTimeTaken(System.currentTimeMillis() - startTime);
            if (Utils.hasElements(sqlResponse.getRows())) {
                sqlResponse.setRowCount(sqlResponse.getRows().size());
            }
            return sqlResponse;
        } catch (ResponseException ex) {
            SQLResponse queryResponse = new SQLResponse();
            queryResponse.setHasErrors(true);
            queryResponse.setErrorMessage(getErrorResponse(ex));
            return queryResponse;
        } catch (IOException ex) {
            logger.warn("Unable to complete SQL query", ex);
            throw new GUSLElasticException(ex);
        }
    }

    private String getErrorResponse(ResponseException ex) {
        try {
            QueryError error = ElasticUtils.getObjectMapper().readValue(ex.getResponse().getEntity().getContent(), QueryError.class);
            logger.info("{}", error);
            return error.getError().getReason();
        } catch (IOException ioEx) {
            return ex.getMessage();
        }
    }

    @Override
    public DeleteResponse deleteDocument(DeleteRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.delete(request, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            logger.warn("Delete document failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public BulkByScrollResponse deleteDocumentsByQuery(DeleteByQueryRequest request) throws GUSLErrorException {
        try {
            return theElasticClient.deleteByQuery(request, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            logger.warn("Delete document by query failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public BulkByScrollResponse updateDocumentByQuery(String indexName, MatchQueryBuilder query) throws GUSLElasticException {
        try {
            UpdateByQueryRequest request = new UpdateByQueryRequest(indexName);
            request.setQuery(query);
            request.setConflicts("proceed");

            return theElasticClient.updateByQuery(request, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            logger.warn("Delete document by query failed", ex);
            throw new GUSLElasticException(ex);
        }
    }

    public String openPointInTime(String index, int minutes) throws GUSLElasticException {
        try {
            OpenPointInTimeRequest request = new OpenPointInTimeRequest(index);
            request.keepAlive(TimeValue.timeValueMinutes(minutes));
            OpenPointInTimeResponse response = theElasticClient.openPointInTime(request, RequestOptions.DEFAULT);
            return response.getPointInTimeId();
        } catch (IOException ex) {
            logger.warn("Unable to open point in time", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public boolean closePointInTime(String pitId) throws GUSLErrorException {
        try {
            ClosePointInTimeRequest request = new ClosePointInTimeRequest(pitId);
            ClosePointInTimeResponse response = theElasticClient.closePointInTime(request, RequestOptions.DEFAULT);
            return response.isSucceeded();
        } catch (IOException ex) {
            logger.warn("Unable to close point in time", ex);
            throw new GUSLElasticException(ex);
        }
    }

    @Override
    public boolean checkConnection() {
        try {
            ClusterHealthRequest request = new ClusterHealthRequest();
            request.waitForActiveShards();
            request.waitForYellowStatus();
            request.timeout("10s");
            ClusterHealthResponse response = theElasticClient.cluster().health(request, RequestOptions.DEFAULT);
            if (!response.isTimedOut()) {
                logger.info("Cluster healthy {}", response);
                return true;
            }
        } catch (IOException ex) {
            logger.info("Unable to connect to elastic cluster {}", theElasticClient.getLowLevelClient().getNodes());
        }

        return false;
    }

    @Override
    public void waitForStartup(Duration duration) throws TimeoutException {
        logger.info("Waiting for the Elastic Connection to start {}", theElasticConfig);
        String[] hosts = theElasticConfig.getArrayOfConnectionPoints();
        IOUtils.waitForService(hosts[0], ElasticConstants.ELASTIC_DEFAULT_PORT, duration, logger);

        while (true) {
            try {
                if (checkConnection()) {
                    return;
                }
            } catch (Throwable ignore) {
            }

            logger.info("Waiting for Elastic {}:{} ...", hosts[0], ElasticConstants.ELASTIC_DEFAULT_PORT);
            Utils.sleep(2000);
        }

    }

    public ElasticConfig getConfig() {
        return theElasticConfig;
    }
}
