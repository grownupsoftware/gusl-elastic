package gusl.elastic.client;

import lombok.CustomLog;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkResponse;

/**
 * @author dhudson
 */
@CustomLog
public class BulkActionListener implements ActionListener<BulkResponse> {

    public BulkActionListener() {
    }

    @Override
    public void onResponse(BulkResponse response) {
        logger.info("onResponse {}", response);
    }

    @Override
    public void onFailure(Exception ex) {
        logger.info("onFailure {}", ex);
    }

}
