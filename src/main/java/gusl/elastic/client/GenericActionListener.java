package gusl.elastic.client;

import java.util.concurrent.CountDownLatch;
import gusl.core.tostring.ToString;
import org.elasticsearch.action.ActionListener;

/**
 *
 * @author dhudson
 */
public class GenericActionListener<T> implements ActionListener<T> {

    private final CountDownLatch theLatch;
    private T theResponse;
    private Exception theException;
    private boolean theResult;

    public GenericActionListener() {
        theLatch = new CountDownLatch(1);
    }

    @Override
    public void onResponse(T response) {
        theResponse = response;
        theResult = true;
        theLatch.countDown();
    }

    @Override
    public void onFailure(Exception ex) {
        theException = ex;
        theResult = false;
        theLatch.countDown();
    }

    public CountDownLatch getCountDownLatch() {
        return theLatch;
    }

    public Exception getException() {
        return theException;
    }

    public T getResponse() {
        return theResponse;
    }

    public boolean getResult() {
        return theResult;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
