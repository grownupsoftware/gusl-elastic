package gusl.elastic.exception;

import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.errors.ElasticErrors;

import java.io.IOException;

/**
 * @author dhudson
 */
@SuppressWarnings("serial")
public class GUSLElasticException extends GUSLErrorException {

    public GUSLElasticException() {
    }

    public GUSLElasticException(IOException ex) {
        super(ElasticErrors.ELASTIC_ERROR.getError(new String[]{ex.getMessage()}), ex);
    }

    public GUSLElasticException(Throwable t) {
        super(ElasticErrors.ELASTIC_ERROR.getError(new String[]{t.getMessage()}), t);
    }

    public GUSLElasticException(String message) {
        super(ElasticErrors.ELASTIC_ERROR.getError(new String[]{message}));
    }
}
