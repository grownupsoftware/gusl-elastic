package gusl.elastic.config;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
public class ElasticMigrateConfig {

    private ElasticConfig elasticConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
