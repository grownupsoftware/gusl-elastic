package gusl.elastic.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.annotations.DocField;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.config.SecretDeserializer;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 26/08/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class ElasticConfig {

    @DocField(description = "Elastic connection points, | (pipe) delimited string")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String connectionPoints;

    @JsonDeserialize(using = SecretDeserializer.class)
    private String username;

    @JsonDeserialize(using = SecretDeserializer.class)
    @ToStringMask
    private String password;

    @JsonDeserialize(using = CoreCountDeserializer.class)
    private int threads;

    @DocField(description = "Default number of shards on an index")
    private int shards;

    @DocField(description = "Default number of replicas")
    private int replicas;

    public String[] getArrayOfConnectionPoints() {
        return connectionPoints.split("\\|");
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
