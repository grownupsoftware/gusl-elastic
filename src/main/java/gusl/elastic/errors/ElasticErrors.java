package gusl.elastic.errors;


import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author grant
 */
public enum ElasticErrors {

    AUTO_FILL_ERROR("EL01 Unable to auto populate fields", "auto.fill.error"),
    ELASTIC_ERROR("EL02 Elastic error: {0}", "elastic.error"),
    NO_PRIMARY_KEY("EL03 No primary ket set on index {0}", "no.primary.key"),
    SECURITY_ERROR("EL04 Security Error {0}", "security.error"),
    UPDATE_PARAMS("EL05 Update with params not supported", "no.update.params"),
    DEFAULT_ERROR("EL06 unable to update {0} with {1}", "default.error");

    private final String message;
    private final String messageKey;

    ElasticErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(Long id) {
        if (id != null) {
            return new ErrorDO(null, message, messageKey, String.valueOf(id));
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(String... params) {

        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
