package gusl.elastic.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.config.ConfigUtils;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.*;
import gusl.elastic.client.ElasticClientImpl;
import gusl.elastic.client.ElasticDelegateImpl;
import gusl.elastic.config.ElasticMigrateConfig;
import gusl.elastic.dao.AbstractDAOElasticImpl;
import gusl.elastic.exception.GUSLElasticException;
import gusl.elastic.model.*;
import gusl.elastic.utils.ElasticConstants;
import gusl.elastic.utils.ElasticUtils;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * https://www.elastic.co/blog/changing-mapping-with-zero-downtime
 *
 * @author dhudson
 */
@CustomLog
@NoArgsConstructor
public class ElasticMigrate {

    public static final String INDEX_BASE_DIRECTORY = "db";
    public static final String MAPPING_FILE_NAME = "mapping.json";

    private static final String HELP_ARG = "h";
    private static final String DEBUG_ARG = "debug";
    private static final String CLEAN_ARG = "clean";
    private static final String MODE_ARG = "mode";
    private static final String DRY_RUN = "dryrun";
    private static final String REPAIR = "repair";
    private static final String LOAD = "load";

    private final ObjectMapper mapper = ElasticUtils.getObjectMapper();

    public void buildClassBasedIndices(ElasticClientImpl elasticClient, boolean debug, boolean dryRun) throws GUSLErrorException {

        List<Class<?>> elasticIndexes = ElasticUtils.getIndexAnnotatedClasses();
        Indices indices = elasticClient.getApplicationIndices();
        HashMap<String, IndexDetail> indexDetails = new HashMap<>();

        // We can have some instances, where we have an application index without a version, these are incorrect and should be deleted.
        for (Index index : indices.getIndices()) {
            IndexDetail detail = new IndexDetail(index);
            indexDetails.put(detail.getAliasName(), detail);
        }

        int created = 0;
        int migrated = 0;

        // Create the index first, so the system can be restarted
        for (Class<?> index : elasticIndexes) {
            ElasticIndex annotation = index.getAnnotation(ElasticIndex.class);
            String alias = getAliasName(annotation);

            IndexDetail detail = indexDetails.get(alias);

            if (detail == null) {
                // Index doesn't exist so let's create it.
                createIndex(elasticClient, debug, annotation, dryRun);
                if (StringUtils.isNotBlank(annotation.staticContent())) {
                    String resourceToLoad = "elastic/instance/com/" + annotation.staticContent();
                    loadStaticContent(elasticClient, index, resourceToLoad);
                }
                created++;
            }
        }

        // Live migrate
        logger.info("Starting live migrate process ..");

        for (Class<?> index : elasticIndexes) {
            ElasticIndex elasticIndex = index.getAnnotation(ElasticIndex.class);
            String alias = getAliasName(elasticIndex);

            IndexDetail detail = indexDetails.get(alias);
            if (detail == null) {
                // Just been created
                continue;
            }

            if (detail.getVersion() != elasticIndex.version()) {
                if (detail.getVersion() < elasticIndex.version()) {
                    // reindex...
                    logger.info("Reindex for {}", alias);
                    // Create the new index
                    createIndex(elasticClient, debug, elasticIndex, dryRun);
                    if (!dryRun) {
                        boolean reindexSuccess = elasticClient.reindexWithSourceQuery(detail.getIndexName(), getIndexName(elasticIndex), null);
                        migrated++;
                        logger.info("Reindex success {}", reindexSuccess);

                        if (reindexSuccess) {
                            // Swap aliases
                            elasticClient.swapAliases(detail.getIndexName(), getIndexName(elasticIndex), alias);
                            // Remove old index
                            elasticClient.deleteIndex(detail.getIndexName());
                        }

                    }
                } else {
                    logger.warn(" Version mismatch code has {}, Elastic has {}, can not degrade an index {}",
                            elasticIndex.version(), detail.getVersion(), detail.getIndexName());
                }
            } else {
                if (debug) {
                    logger.info("No reindex required for {}", alias);
                }
            }
        }

        if (debug) {
            logger.info("Current Aliases");
            ElasticAliases aliases = elasticClient.getApplicationAliases();

            for (ElasticAlias alias : aliases.getAliases()) {
                logger.info("{}", alias);
            }
        }

        logger.info("Migrate complete, Created {}, Migrated {}", created, migrated);
    }

    private void swapAliasToOldOnMigrateFailure(ElasticClientImpl elasticClient, ElasticIndex elasticIndex, String alias, IndexDetail detail) throws GUSLErrorException {
        logger.info("Swapping aliases for {} - old: {} - new: {}", alias, getIndexName(elasticIndex), detail.getIndexName());
        elasticClient.swapAliases(getIndexName(elasticIndex), detail.getIndexName(), alias);
    }

    private void swapAliasToNew(ElasticClientImpl elasticClient, ElasticIndex elasticIndex, String alias, IndexDetail detail) throws GUSLErrorException {
        logger.info("Swapping aliases for {} - old: {} - new: {}", alias, detail.getIndexName(), getIndexName(elasticIndex));
        elasticClient.swapAliases(detail.getIndexName(), getIndexName(elasticIndex), alias);
    }

    private void createIndex(ElasticClientImpl elasticClient, boolean debug, ElasticIndex elasticIndex, boolean dryRun) throws GUSLElasticException {
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"mappings\": {");
        builder.append("\"dynamic\": \"false\",");
        builder.append("\"properties\": {");

        Set<Field> fields = ClassUtils.getFieldsFor(elasticIndex.type(), false);
        builder.append(processFields(fields, elasticIndex));
        builder.append("}}}");
        String indexName = getIndexName(elasticIndex);

        //logger.info("{}", builder.toString());

        if (debug) {
            try {
                Object jsonObject = mapper.readValue(builder.toString(), Object.class);
                logger.info("Mapping [{}]: \n{}", indexName, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject));
            } catch (IOException ex) {
                logger.warn(ex);
            }
        }

        if (dryRun) {
            logger.info("Index {} created", indexName);
        } else {
            // now call ES
            try {
                boolean indexCreated = elasticClient.createIndex(indexName, builder.toString(), getAliasName(elasticIndex));
                if (!indexCreated) {
                    logger.warn("Index {} not created", indexName);
                } else {
                    logger.info("Index {} created", indexName);
                }
            } catch (GUSLErrorException ex) {
                logger.error("Failed to create index for : {}", indexName, ex);
            }
        }
    }

    private String getIndexName(ElasticIndex index) {
        StringBuilder builder = new StringBuilder();
        builder.append(index.index());

        if (index.rotate()) {
            builder.append(ElasticConstants.DATE_PREFIX);
            builder.append(DateFormats.formatDate(ElasticConstants.ROTATION_DATE_FORMAT, new Date()));
        }

        builder.append(ElasticConstants.VERSION_PREFIX);
        builder.append(index.version());

        return builder.toString();
    }

    private String getAliasName(ElasticIndex index) {
        return index.index();
    }

    /**
     * If there are Elastic Types in the Object, then map them as properties,
     * else just leave it as type object.
     *
     * @param field
     * @return
     */
    private String objectRepresentation(Field field, ElasticIndex index) throws GUSLElasticException {

        Class clazz;
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType parametrized = (ParameterizedType) field.getGenericType();
            clazz = (Class<?>) parametrized.getActualTypeArguments()[0];
        } else {
            clazz = field.getType();
        }

        StringBuilder builder = new StringBuilder();
        Set<Field> fields = ClassUtils.getFieldsFor(clazz, false);
        if (fields.isEmpty()) {
            builder.append("\"type\": \"object\"");
        } else {
            builder.append("\"properties\": {");
            builder.append(processFields(fields, index));
            builder.append("}");
        }
        return builder.toString();
    }

    private String processFields(Set<Field> fields, ElasticIndex index) throws GUSLElasticException {
        StringBuilder builder = new StringBuilder();
        int cnt = 0;
        for (Field field : fields) {
            ElasticField elasticField = field.getAnnotation(ElasticField.class);

            if (elasticField != null) {
                if (cnt > 0) {
                    builder.append(",");
                }

                // Field name mapped
                builder.append("\"").append(ElasticUtils.getElasticFieldName(field)).append("\": {");

                switch (elasticField.type()) {
                    case MULTI:
                        builder.append("\"type\": \"").append("text").append("\",");
                        builder.append("\"fields\": {");
                        builder.append("\"raw\": {");
                        builder.append("\"type\": \"keyword\"}}");
                        break;
                    case OBJECT:
                    case NESTED:
                        builder.append(objectRepresentation(field, index));
                        break;
                    case DATE:
                        if (field.getType() == LocalDateTime.class) {
                            logger.warn(" .............  {} LocalDateTime not supported, please use ZonedDateTime ........", field);
                            throw new GUSLElasticException("LocalDateTime not supported");
                        }
                        if (field.getType() == LocalDate.class) {
                            logger.warn(" .............  {} LocalDate not supported, please use ZonedDateTime ........", field);
                            throw new GUSLElasticException("LocalDate not supported");
                        }
//                        builder.append("\"type\": \"date\",");
//                        builder.append("\"format\": \"yyyy-MM-dd'T'HH:mm:ss.SSSXXX\"");
//                        break;
                    default:
                        builder.append("\"type\": \"").append(getElasticFieldType(elasticField.type())).append("\"");
//                        String nullValue = getNullValue(index, elasticField);
//                        if (nullValue != null) {
//                            builder.append(",\"null_value\": \"");
//                            builder.append(nullValue);
//                            builder.append("\"");
//                        }
                }

                builder.append("}");
                cnt++;
            }
        }

        return builder.toString();
    }

    private String getNullValue(ElasticIndex index, ElasticField field) {
//        if (StringUtils.isNotBlank(field.nullValue())) {
//            return field.nullValue();
//        }
//
//        if (StringUtils.isNotBlank(index.nullValue())) {
//            return index.nullValue();
//        }

        return null;
    }

    private String getElasticFieldType(ESType type) {
        return type.name().toLowerCase();
    }

    private void manualMappings(ElasticClientImpl elasticClient) throws IOException, GUSLErrorException {
        Set<String> indices = ResourceLocator.findResourceNames(INDEX_BASE_DIRECTORY);
        for (String index : indices) {
            Set<String> files = ResourceLocator.findResourceNames(INDEX_BASE_DIRECTORY + "/" + index);
            if (files.contains(MAPPING_FILE_NAME)) {
                String fileName = INDEX_BASE_DIRECTORY + "/" + index + "/" + MAPPING_FILE_NAME;
                boolean indexCreated = elasticClient.createIndex(ElasticConstants.ELASTIC_INDEX_PREFIX + index, IOUtils.getResourceAsFile(fileName, ElasticMigrate.class.getClassLoader()));
                if (!indexCreated) {
                    logger.warn("Index not created");
                } else {
                    logger.info("Index {} created", ElasticConstants.ELASTIC_INDEX_PREFIX + index);
                }

            }
        }
    }

    public void truncateAll(ElasticClientImpl elasticClient, boolean dryRun) throws GUSLErrorException {
        Indices indices = elasticClient.getApplicationIndices();
        for (Index index : indices.getIndices()) {
            logger.info("Removing index {}", index.getIndex());
            try {
                if (!dryRun) {
                    elasticClient.deleteIndex(index.getIndex());
                }
            } catch (GUSLElasticException ex) {
                logger.warn("Unable to remove index {}", index.getIndex(), ex);
            }
        }
    }

    public void repair(ElasticClientImpl elasticClient, boolean dryRun) throws GUSLErrorException {

        Indices indices = elasticClient.getAllIndices();

        // We can have some instances, where we have an LM index without a version, these are incorrect and should be deleted.
        for (Index index : indices.getIndices()) {
            IndexDetail detail = new IndexDetail(index);
            if (detail.getVersion() == 0) {
                logger.warn("Incorrect index has there is no version, removing {}", detail.getIndexName());
                if (!dryRun) {
                    elasticClient.deleteIndex(detail.getIndexName());
                }
            } else if (detail.getAliasName() == null) {
                logger.warn("Alias not set for index {}, suggesting {}", detail.getIndexName(), detail.getSuggestedAlias());
                if (!dryRun) {
                    elasticClient.addAlias(index.getIndex(), detail.getSuggestedAlias());
                }
            }

        }
    }

    private void loadStaticContent(ElasticClientImpl elasticClient, Class<?> clazzImpl, String resourceToLoad) {
        try {
            AbstractDAOElasticImpl indexAccess = createIndexAccess(elasticClient, clazzImpl);
            int rowsLoaded = indexAccess.loadResource(resourceToLoad);
            logger.info("Loaded {} rows into {},", rowsLoaded, indexAccess.getIndexName());
        } catch (Throwable ex) {
            logger.warn("Unable to load static content for {}", resourceToLoad, ex);
        }
    }

    private void loadStaticContent(ElasticClientImpl elasticClient, String location) {
        try {
            File fileLocation = new File(location);
            if (!fileLocation.exists()) {
                logger.warn("Location {} doesn't exist", location);
                return;
            }

            List<File> filesToLoad;

            if (fileLocation.isFile()) {
                filesToLoad = Arrays.asList(new File(location));
            } else {
                filesToLoad = new ArrayList<>();
                filesToLoad.addAll(Arrays.asList(fileLocation.listFiles()));
            }

            logger.info("Resources to Load {}", filesToLoad);

            MigrateHelper helper = new MigrateHelper();

            for (File resource : filesToLoad) {
                logger.info("Resource {}", resource);
                String name = resource.getName();
                String indexName = StringUtils.getBaseName(name);
                logger.info("Name {}, indexName {}", name, indexName);
                IndexMedata metadata = helper.getMetadata(indexName);
                if (metadata == null) {
                    logger.warn("Can't find index {}, for resource {}", indexName, resource);
                    continue;
                }

                AbstractDAOElasticImpl indexAccess = createIndexAccess(elasticClient, metadata.getAccessClass());
                int rowsLoaded = indexAccess.loadFile(resource);
                logger.info("Loaded {} rows into {},", rowsLoaded, indexAccess.getIndexName());
            }
        } catch (Throwable ex) {
            logger.warn("Unable to locate {}", location, ex);
        }
    }

    private AbstractDAOElasticImpl createIndexAccess(ElasticClientImpl client, Class<?> clazzImpl) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        AbstractDAOElasticImpl indexAccess = (AbstractDAOElasticImpl) ClassUtils.getNewInstance(clazzImpl);
        ElasticDelegateImpl delegate = new ElasticDelegateImpl();
        delegate.setClient(client);
        indexAccess.setDelegate(delegate);
        return indexAccess;
    }

    void bootstrap(CommandLine commandLine) {
        try {
            ElasticMigrateConfig config = ConfigUtils.loadConfig(ElasticMigrateConfig.class);

            boolean isProd = false;

            String constructName = System.getenv("CONSTRUCT");

            if (!StringUtils.isBlank(constructName)) {
                if (constructName.toLowerCase().contains("prod")) {
                    logger.info("--- Production Instance  --- ");
                    isProd = true;
                }
            }

            try (ElasticClientImpl elasticClient = new ElasticClientImpl()) {
                elasticClient.configure(config.getElasticConfig());

                try {
                    if (commandLine.hasOption(DRY_RUN)) {
                        logger.info("Dryrun option detected, all actions simulated");
                    }

                    if (commandLine.hasOption(REPAIR)) {
                        repair(elasticClient, commandLine.hasOption(DRY_RUN));
                        return;
                    }

                    if (commandLine.hasOption(CLEAN_ARG)) {
                        if (isProd) {
                            logger.warn("Can't TRUNCATE a PRODUCTION database");
                            return;
                        }
                        // Lets the existing indexes
                        truncateAll(elasticClient, commandLine.hasOption(DRY_RUN));
                    }

                    if (commandLine.hasOption(LOAD)) {
                        loadStaticContent(elasticClient, commandLine.getOptionValue(LOAD));
                    } else {
                        // do class based mappings
                        buildClassBasedIndices(elasticClient, commandLine.hasOption(DEBUG_ARG), commandLine.hasOption(DRY_RUN));
                    }
                } catch (GUSLErrorException ex) {
                    logger.warn("Unable to process, elastic exception ", ex);
                }
            }
        } catch (IOException ex) {
            logger.warn("Unable to load config file", ex);
        }
    }

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption(DEBUG_ARG, "Enable debugging");
        options.addOption(HELP_ARG, "Help");
        options.addOption(CLEAN_ARG, "Clean the indices");
        options.addOption(MODE_ARG, true, "Casanova Operations mode");
        options.addOption(DRY_RUN, "Dry run, simulate actions");
        options.addOption(REPAIR, "Check and repair indices");
        options.addOption(LOAD, true, "Load data from the given location");

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException ex) {
            logger.error("Command Line Exception " + ex.getLocalizedMessage());
            printHelp(options);
            return;
        }

        if (commandLine.hasOption(HELP_ARG)) {
            System.err.println("No arguments supplied");
            printHelp(options);
            return;
        }

        IdGenerator.setIfNotSet();

        new ElasticMigrate().bootstrap(commandLine);
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("elastic-migrate", options);
    }

}
