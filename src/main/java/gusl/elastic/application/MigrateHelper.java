package gusl.elastic.application;

import gusl.elastic.utils.ElasticUtils;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dhudson
 * @since 19/01/2022
 */
@Getter
public class MigrateHelper {

    private final List<Class<?>> elasticIndexes;
    private final Map<String, IndexMedata> metadataMap;

    MigrateHelper() {
        elasticIndexes = ElasticUtils.getIndexAnnotatedClasses();
        metadataMap = new HashMap<>(elasticIndexes.size());
        elasticIndexes.forEach(aClass -> {
            IndexMedata metadata = new IndexMedata(aClass);
            metadataMap.put(metadata.indexName(), metadata);
        });
    }

    IndexMedata getMetadata(String indexName) {
        return metadataMap.get(indexName);
    }
}
