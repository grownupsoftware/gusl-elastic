package gusl.elastic.application;

import gusl.elastic.model.ElasticIndex;
import lombok.Getter;

/**
 * @author dhudson
 * @since 19/01/2022
 */
@Getter
public class IndexMedata {

    private final Class<?> accessClass;
    private final ElasticIndex annotation;

    IndexMedata(Class<?> clazz) {
        accessClass = clazz;
        annotation = clazz.getAnnotation(ElasticIndex.class);
    }

    public String indexName() {
        return annotation.index();
    }

    public int getVersion() {
        return annotation.version();
    }
}
