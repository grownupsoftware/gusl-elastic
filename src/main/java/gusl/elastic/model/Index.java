package gusl.elastic.model;

/**
 * @author dhudson
 */

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.cluster.metadata.MappingMetadata;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Index {

    private String index;
    private String uuid;
    private int shards;
    private int replicas;
    private List<AliasMetadata> aliases;
    private String alias;
    private MappingMetadata mappings;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
