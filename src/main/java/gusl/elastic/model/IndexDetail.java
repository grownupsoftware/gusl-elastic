package gusl.elastic.model;

import gusl.core.tostring.ToString;
import gusl.core.utils.DateFormats;
import gusl.elastic.utils.ElasticConstants;
import lombok.CustomLog;

import java.text.ParseException;
import java.util.Date;

/**
 * @author dhudson
 */
@CustomLog
public class IndexDetail {

    // Normal name of the index
    private String aliasName;
    // Versioned name of the index
    private String indexName;
    // Version
    private int version;
    // Date of index (if rotate)
    private Date date;

    private String suggestedAlias;

    public IndexDetail() {
    }

    public IndexDetail(Index index) {
        parseIndex(index);
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSuggestedAlias() {
        return suggestedAlias;
    }

    public void setSuggestedAlias(String suggestedAlias) {
        this.suggestedAlias = suggestedAlias;
    }

    private void parseIndex(Index index) {
        // This is the alias as to what elastic thinks
        aliasName = index.getAlias();
        indexName = index.getIndex();

        int pos = indexName.lastIndexOf(ElasticConstants.VERSION_PREFIX);
        if (pos > 0) {
            version = Integer.parseInt(indexName.substring(pos + 2));
            suggestedAlias = indexName.substring(0, pos);
        } else {
            suggestedAlias = indexName;
        }

        int rotate = indexName.lastIndexOf(ElasticConstants.DATE_PREFIX);
        if (rotate > 0) {
            String possibleDate = indexName.substring(rotate + 2, rotate + 12);
            try {
                date = DateFormats.getFormatFor(ElasticConstants.ROTATION_DATE_FORMAT).parse(possibleDate);
            } catch (ParseException ex) {
                logger.warn("Unable to parse date {}", possibleDate, ex);
            }
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
