package gusl.elastic.model;

import gusl.core.tostring.ToString;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.common.settings.Settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dhudson
 */
public class Indices {

    private static final String INDEX_CREATION_DATE = "index.creation_date";
    private static final String NUMBER_OF_REPLICAS = "index.number_of_replicas";
    private static final String NUMBER_OF_SHARDS = "index.number_of_shards";
    private static final String INDEX_PROVIDED_NAME = "index.provided_name";
    private static final String INDEX_UUID = "index.uuid";
    private static final String INDEX_CREATED_DATE = "index.version.create";

    private List<Index> indices;

    public Indices() {
    }

    public List<Index> getIndices() {
        return indices;
    }

    public void setIndices(List<Index> indices) {
        this.indices = indices;
    }

    public void populate(GetIndexResponse response) {

        indices = new ArrayList<>(response.getIndices().length);

        for (String indexName : response.getIndices()) {
            Settings settings = response.getSettings().get(indexName);
            List<AliasMetadata> aliases = response.getAliases().get(indexName);

            Index index = new Index();
            index.setIndex(indexName);
            index.setUuid(settings.get(INDEX_UUID));
            index.setShards(settings.getAsInt(NUMBER_OF_SHARDS, null));
            index.setReplicas(settings.getAsInt(NUMBER_OF_REPLICAS, null));
            index.setMappings(response.getMappings().get(indexName));

            if (aliases != null) {
                index.setAliases(aliases);
                if (!aliases.isEmpty()) {
                    index.setAlias(aliases.get(0).alias());
                }
            }
            indices.add(index);
        }

        Collections.sort(indices, Comparator.comparing(Index::getIndex));
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
