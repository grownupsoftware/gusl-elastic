package gusl.elastic.model;

import gusl.core.tostring.ToString;
import org.elasticsearch.cluster.metadata.AliasMetadata;

import java.util.*;

/**
 * Lotto Indexes can only have one alias
 *
 * @author dhudson
 */
public class ElasticAliases {

    private List<ElasticAlias> aliases;

    public ElasticAliases() {
    }

    public List<ElasticAlias> getAliases() {
        return aliases;
    }

    public void setAliases(List<ElasticAlias> aliases) {
        this.aliases = aliases;
    }

    public void populate(Map<String, Set<AliasMetadata>> data) {
        aliases = new ArrayList<>(data.size());

        for (Map.Entry<String, Set<AliasMetadata>> entry : data.entrySet()) {
            Set<AliasMetadata> aliasData = entry.getValue();
            if (!aliasData.isEmpty()) {
                AliasMetadata[] array = new AliasMetadata[aliasData.size()];
                array = aliasData.toArray(array);
                ElasticAlias eAlias = new ElasticAlias();
                eAlias.setAlias(array[0].alias());
                eAlias.setIndex(entry.getKey());
                aliases.add(eAlias);
            }
        }

        Collections.sort(aliases, Comparator.comparing(ElasticAlias::getAlias));

    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
