package gusl.elastic.model;

import gusl.core.tostring.ToString;

/**
 * @author dhudson
 */
public class ElasticAlias {

    private String alias;
    private String index;

    public ElasticAlias() {
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
