package gusl.elastic.query;

import gusl.core.tostring.ToString;
import lombok.*;

/**
 * @author dhudson
 * @since 09/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SQLRequest {

    private String query;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
