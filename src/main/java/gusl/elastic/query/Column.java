package gusl.elastic.query;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author dhudson
 * @since 09/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Column {

    private String name;
    private String type;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
