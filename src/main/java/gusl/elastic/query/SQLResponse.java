package gusl.elastic.query;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 * @since 09/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class SQLResponse {

    private int rowCount;
    private long timeTaken;
    private boolean hasErrors;
    private String errorMessage;
    private List<Column> columns;
    private List<Object> rows;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
