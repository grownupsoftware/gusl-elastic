package gusl.elastic.query;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 09/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class QueryError {

    private int status;
    private Error error;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
