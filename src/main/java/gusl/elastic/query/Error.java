package gusl.elastic.query;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 * @since 09/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class Error {
    private String type;
    private String reason;
    private List<Error> rootCause;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
